/*
ENEL 417 Capstone Project
Zhengyang Zhang
This program is to measure temperature and SPO2 with two sensors: MLX90614 and MAX30100.
MLX90614 uses IIC protocal which intended to allow multiple "peripheral" digital integrated circuits ("chips") to communicate with one or more "controller" chips
MAX30100 uses UART which generates its data clock internally to the microcontroller and synchronizes that clock with the data stream by using the start bit transition.


*/