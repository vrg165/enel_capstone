
#include "my_mlx90614.h"

unsigned char DataH,DataL,Pecreg;

float My_MLX90614_ReadObjectTemp(void)
{
    IIC_Start();
    IIC_Send_Byte(0x00); //Send SlaveAddress
    IIC_Send_Byte(0x07); //Send Command
    IIC_Start();
    IIC_Send_Byte(0x01);
    DataL=IIC_Recv_Byte(IIC_BIT_ACK);
    DataH=IIC_Recv_Byte(IIC_BIT_ACK);
    Pecreg=IIC_Recv_Byte(IIC_BIT_NOACK);
    IIC_Stop();
    return (DataH*256+DataL)*0.02-273.15;
}


