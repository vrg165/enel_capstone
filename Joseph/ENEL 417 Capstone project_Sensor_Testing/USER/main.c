#include "my_include.h"

char dis0[32];
float readTemp = 0;// initialize temperature setting

int main(void)
{
    LCD1602_Init();
    My_MAX301xx_Init(USART2);
    My_MLX90614_Init();
    
     while(1)
    {
        if(myReadFlag_tick==true)
        {
            myReadFlag_tick = false;
            readTemp = My_MLX90614_ReadObjectTemp();//read object temperature
            My_MAX301xx_Uart_GetSPO2(USART3);//send command to get data
            sprintf(dis0," Temp:%4.1fC    ",readTemp);//transform data format
            LCD1602_Write_String(0,0,dis0);
            sprintf(dis0," SPO2:%03d ",SPO2_max301xx);//transform data format
            LCD1602_Write_String(0,1,dis0);
        }
    }
}
