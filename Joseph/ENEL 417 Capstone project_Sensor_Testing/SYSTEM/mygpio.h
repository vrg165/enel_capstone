#ifndef __MYGPIO_H
#define __MYGPIO_H

typedef enum
{ 
PA0=0, PA1, PA2, PA3, PA4, PA5, PA6, PA7, PA8, PA9, PA10, PA11, PA12, PA13, PA14, PA15,
  PB0, PB1, PB2, PB3, PB4, PB5, PB6, PB7, PB8, PB9, PB10, PB11, PB12, PB13, PB14, PB15, 
  PC0, PC1, PC2, PC3, PC4, PC5, PC6, PC7, PC8, PC9, PC10, PC11, PC12, PC13, PC14, PC15, 
  PIN_NULL=0xff
}MyPinDef;

#include "my_include.h"
#ifndef BIT_ADDR
#endif 

#ifndef GPIOA_ODR_Addr
#endif 

#ifndef GPIOA_IDR_Addr
#endif 

#define GET_PORT_GPIO(n)        (GPIO_TypeDef *)(GPIOA_BASE+0x0400UL*((n)>>4))
#define GET_PIN_GPIO(n)         (GPIO_Pin_0<<((n)&0x0f))

//IO operating slow( at 72Mhz on STM32F103 is about 0.85us). This makes it more flexable
#define PinRead(n)              BIT_ADDR(GPIOA_IDR_Addr+0x400*((n)>>4),((n)&0x0f))
#define PinOut(n)               BIT_ADDR(GPIOA_ODR_Addr+0x400*((n)>>4),((n)&0x0f))
#define PinWrite                PinOut
#define PinSet(n)               PinOut(n)=1
#define PinReset(n)             PinOut(n)=0

void GPIO_Pin_Init(MyPinDef pin,GPIOMode_TypeDef Mode);
void GPIO_WriteHigh(GPIO_TypeDef* GPIOx,u8 dat);
void GPIO_WriteLow(GPIO_TypeDef* GPIOx,u8 dat);
u16 My_GPIO_GetVersion(void);

#endif
