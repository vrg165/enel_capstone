#ifndef __MY_SYSTICK_H
#define __MY_SYSTICK_H
#include "my_include.h"

#if defined (__RT_THREAD_H__)
#else
#define FLAG_READ_SYSTICK           1
#define FLAG_SEND_SYSTICK           1

#if !defined (USE_HAL_DRIVER)
#define TICK_PER_SECOND             1000    //1000 tick per second
#define My_SysTick_GetPeriod()      ((float)1000/TICK_PER_SECOND)
#endif

void My_SysTick_Init(void);
u32 My_SysTick_GetTicks(void);
#endif

#define My_SysTick_GetCounter()     (SysTick->LOAD + SysTick->VAL +1)
#define My_SysTick_GetSeconds()     (My_SysTick_GetTicks()/TICK_PER_SECOND)
#define SysTickOfSeconds(n)         (n*TICK_PER_SECOND)// tick inturupt in n seconds
#define SysTickSecPassed(m,n)       ((My_SysTick_GetTicks()-m)>=SysTickOfSeconds(n))// timer interupts in n seconds, m is the TimerTicks in main
#define SysTickSecArrive(m,n)       ((My_SysTick_GetTicks()-m)==SysTickOfSeconds(n))// timer interupts in n seconds, m is the TimerTicks in main

#if FLAG_READ_SYSTICK>0
extern bool myReadFlag_tick;//read data sign
#endif
#if	FLAG_SEND_SYSTICK>0
extern bool mySendFlag_tick;//send data sign
#endif

u16 My_SysTick_GetVersion(void);
#endif
