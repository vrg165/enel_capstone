
#ifndef __MY_INCLUDE_H
#define __MY_INCLUDE_H

/******************************header files*******************************************/
#include "my_sysdef.h"
#ifndef  USE_RT_THREAD
#include "my_systick.h"
#endif
#include "delay.h"
#include "mygpio.h"
#include "usart.h"
#include "my_iic.h"
#include "my_lcd1602.h"
#include "my_max301xx_uart.h"
#include "my_mlx90614.h"


#endif
