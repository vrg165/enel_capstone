#include "my_sysdef.h"
#include "my_include.h"

#define VERSION     41

bool StateMachine_GetStr(char *str,u8 msgByte,u8 *state)
{
    if(msgByte == *(str+*state))
    {
        *state = *state + 1;
        if(*state == strlen(str))
        {
            *state = 0;
            return true; //return true if they are all equal
        }
    }
    else
    {
        *state = 0;
    }
    return false; //return false if they are all equal
}
