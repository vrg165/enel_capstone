#ifndef __USART_H
#define __USART_H
#include "my_include.h"


#define EN_USART3               1//Enable USART 3
#define USART3_IT_RXNE          1
#define USART3_IT_TXE           0
#define USART3_IT_IDLE          0

//Pin definition from 387 board
//#define PIN_USART1_TX           PB6
//#define PIN_USART1_RX           PB7
//#define PIN_USART2_TX           PA2
//#define PIN_USART2_RX           PA3
#define PIN_USART3_TX           PB10
#define PIN_USART3_RX           PB11


#if USART3_IT_IDLE>0
#define LEN_BUF_USART3_RX       256
void OnUSART3_ReceiveBuff(u8 *buf,u16 len);
#endif
//增加宏定义兼容旧版本
#define SetPrintfUSART          My_USART_SetPrintfUSART
#define GetPrintfUSART          My_USART_GetPrintfUSART
#define USARTSendByte           My_USART_SendByte
#define USARTSendBytes          My_USART_SendBytes
#define USARTSendString         My_USART_SendString
#define USART_printf            My_USART_printf
#define USARTx_Init             My_USART_Init

extern USART_TypeDef* USART_Printf;

void My_USART_SetReceiveByteHook(USART_TypeDef* USARTx,void ( *OnReceiveByte)( u8 Res ));
void My_USART_SendByte(USART_TypeDef* USARTx, uint8_t Data);
void My_USART_SendBytes(USART_TypeDef* USARTx, const uint8_t *Data, uint16_t length);
void My_USART_SendString(USART_TypeDef* USARTx, const char *str);
void My_USART_printf(USART_TypeDef* USARTx,const char *format, ...);
void My_USART_Init(USART_TypeDef* USARTx, u32 baud);
u16 My_USART_GetVersion(void);

#endif


