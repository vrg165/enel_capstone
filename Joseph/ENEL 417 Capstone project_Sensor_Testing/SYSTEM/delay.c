#include "delay.h"

//initialize DWT
void DWT_Init(void)
{
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0u;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}


void delay_us(u32 usCount)
{
    u32 tCnt, tDelayCnt;
    u32 tStart;
    if(usCount==0)
    {
        return;
    }
    tStart = DWT->CYCCNT; //value of the start
    tCnt = 0;
    tDelayCnt = usCount * (SystemCoreClock / 1000000)-12; //count needed -32 is close to the actual value

    while(tCnt < tDelayCnt)
    {
        tCnt = DWT->CYCCNT - tStart; 
    }
}
