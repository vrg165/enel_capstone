#include "usart.h"
#define VERSION         27

#if 1
FILE __stdout;
USART_TypeDef* USARTx_Printf = USART1;
#endif 

void OnUSART_ReceiveByte(u8 Res)
{   }
void ( *OnUSART3_ReceiveByte )( u8 Res ) = OnUSART_ReceiveByte;

void My_USART_SetReceiveByteHook(USART_TypeDef* USARTx,void ( *OnReceiveByte)( u8 Res ))
{

    if(USARTx==USART3)
    {
        OnUSART3_ReceiveByte = OnReceiveByte;
    }

}

void My_USART_SendByte(USART_TypeDef* USARTx, uint8_t dat)
{
    while((USARTx->SR&USART_SR_TC)==0);//wait for transmission to complete
    USART_SendData(USARTx,dat);
}

void My_USART_SendBytes(USART_TypeDef* USARTx, const uint8_t *dat, uint16_t length)
{
    while(length--)//length of data is not 0
    {
        USARTSendByte(USARTx,*dat);
        dat++;
    }
}

void My_USART_SendString(USART_TypeDef* USARTx, const char *str)
{
    while(*str!='\0')
    {
        USARTSendByte(USARTx,*str);
        str++;
    }
}


void My_USART_Init(USART_TypeDef* USARTx, u32 baud)
{
    //GPIO settings
    USART_InitTypeDef USART_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

#if EN_USART3   //if enable usart3
     if(USARTx == USART3)
    {
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);// enable USART3
        GPIO_Pin_Init(PIN_USART3_TX,GPIO_Mode_AF_PP);
        GPIO_Pin_Init(PIN_USART3_RX,GPIO_Mode_IN_FLOATING);//floating
        NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;//subpriority =2
    }
#endif

    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;// Enable IRQ
    NVIC_Init(&NVIC_InitStructure);	

    //USART initialize
    USART_InitStructure.USART_BaudRate = baud;//usually sets to 9600;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;//8 bit in length data format
    USART_InitStructure.USART_StopBits = USART_StopBits_1;// stop bit
    USART_InitStructure.USART_Parity = USART_Parity_No;//parity check
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//no hardware flow control 
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//receivce and trasmit mode

    USART_DeInit(USARTx);
    USART_Init(USARTx, &USART_InitStructure); 

#if EN_USART3   //if usart3 is enabled 
     if(USARTx == USART3)
    {
    #if USART3_IT_RXNE>0
        USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);
    #endif

    }
#endif
    USART_Cmd(USARTx, ENABLE);//enable usart
}


#if EN_USART3  

void USART3_IRQHandler(void)// usart3 Interrupt request
{
    u8 Res;
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)// transmission stoped
    {
        Res = USART_ReceiveData(USART3);// read what's received
        OnUSART3_ReceiveByte(Res);
    }
} 

#endif

u16 My_USART_GetVersion(void)
{
    return VERSION;
}
