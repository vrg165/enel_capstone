
#ifndef __DELAY_H
#define __DELAY_H
#include "my_include.h" 

#define delay_init  DWT_Init

void DWT_Init(void);
void delay_us(u32 usCount);
#if !defined(USE_RT_THREAD)
void delay_ms(u16 msCount);
#endif
void delay_sec(u8 secCount);

#endif





























