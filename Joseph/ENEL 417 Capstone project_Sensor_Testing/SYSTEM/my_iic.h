
#ifndef __MY_IIC_H
#define __MY_IIC_H
#include "my_include.h"

typedef enum
{
    IIC_BIT_ACK = 0,
    IIC_BIT_NOACK = 1
} Bit_IICACK;

#define PIN_IIC_SCL     PA1    //SCL pin PA1
#define PIN_IIC_SDA     PA0			//SDA pin PA0

void IIC_Init(void);//initiate I2C pin
void IIC_Start(void);//send I2c start signal
void IIC_Stop(void);//send I2c stop signal

u8 IIC_Send_Byte(u8 sendDat);//IIC send 1 bit
u8 IIC_Recv_Byte(Bit_IICACK ack);//IIC receive 1 bit

void IIC_WriteOneByte(u8 SlaveAddress, u8 REG_Address,u8 REG_data);
void IIC_WriteBytes(u8 SlaveAddress, u8 REG_Address,u8 *buf,u16 len);
void IIC_WriteString(u8 SlaveAddress, u8 REG_Address,char *buf);

u8 IIC_ReadOneByte(u8 SlaveAddress, u8 REG_Address);
void IIC_ReadBytes(u8 SlaveAddress, u8 REG_Address,u8 *buf,u16 len);
#endif


