//
//	ENEL Capstone Project 
//	PYQ - Motion Sensor Test Code
//
//	Vincent Grabowski, March 2021
//
//************************************

#include "stm32f10x.h"
#include <stdint.h>
#include "Clocks.h"
#include "Utility.h"
#include "LCD_utility.h"

int main()
{
	ClockInit();
	PB_IO_init();
	led_IO_init();
	
	PYQ_IO_Init();
	
	//delay(60000);
	
	//Set Register Bits for Mostion Sensor
	uint32_t reg_Bits = 0x0020E931;
	set_Serin(reg_Bits);
	
	uint16_t PB_VAL;	//Push Button Value
	uint8_t DL_VAL;		//Direct Link Pin Value
	set_led(0xF);
	//GPIOB->ODR &= ~GPIO_ODR_ODR10;
	
	
	while(1)
	{
		DL_VAL = MS_DL_VAL();
		if(DL_VAL == 0x01)	//DL_VAL equals 1 if motion has been detected
		{
			set_led(0x07); 	//turn on LED4 on test board
			if(Read_PB() != PB_VAL)	//if a pushbutton has been pressed
			{
				PB_VAL = Read_PB();
				//if green button pushed, send high voltage to transistor base
				//drawing Direct Link pin to 0V for 100 micro-seconds. reset LED4
				if(PB_VAL == 0x07) 
				{
					GPIOB->ODR |= GPIO_ODR_ODR10;
					delay(600);
					GPIOB->ODR &= ~GPIO_ODR_ODR10;
					set_led(0x0F);
				}
			}
				
		}
	}
	
}