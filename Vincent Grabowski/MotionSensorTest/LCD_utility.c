/***************************************
*LCD_utility.h 
*
*Vincent Grabowski
*
*
****************************************/

#include "stm32f10x.h"
#include "LCD_utility.h"


void commandToLCD(uint8_t data)
{
	GPIOB->BSRR = LCD_CM_ENA; //RS low, E high
	
	// GPIOC->ODR = data; //BAD: may affect upper bits on port C
	
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	
	delay(8000);
	
	GPIOB->BSRR = LCD_CM_DIS; //RS low, E low
	
	delay(80000);
	
}

void dataToLCD(uint8_t data)
{
	GPIOB->BSRR = LCD_DM_ENA; //RS low, E high
	
	// GPIOC->ODR = data; //BAD: may affect upper bits on port C
	
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	
	delay(8000);
	
	GPIOB->BSRR = LCD_DM_DIS; //RS low, E low
	
	delay(80000);
}	

void display32int(uint32_t displayValue)
{
	int i;
	uint32_t shiftedDisplayValue;
	uint8_t outputValue;
	dataToLCD(0x30);
	dataToLCD(0x78);
	
	for(i=28; i >= 0; i = (i - 4))
	{
		shiftedDisplayValue = ((displayValue >> i) & 0x0F);
		outputValue = (convertToASCII(shiftedDisplayValue));
		dataToLCD(outputValue);
	}
	
	dataToLCD(0x20);
}

void LCD_init(void)
{
	//start clocks for port C and B
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN;
	
	commandToLCD(0x38);
	commandToLCD(0x38);
	commandToLCD(0x38);
	commandToLCD(0x38);
	commandToLCD(0x0F);
	commandToLCD(0x01);
	commandToLCD(0x06);
	
}

void LCD_IO_init(void)
{
	//start clocks for port C and B
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN;
	
	GPIOB->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE5 ;
  GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF5 ;
	
	GPIOC->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3 \
						 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7 ;
	GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF2 & ~GPIO_CRL_CNF3 \
						 & ~GPIO_CRL_CNF4 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF6 & ~GPIO_CRL_CNF7 ;
	delay(80000);
	LCD_init();
	
}

uint8_t convertToASCII(uint16_t data)
{
	uint8_t temp = (data & 0x0F);
	
	if(temp < 0x0A)
	{
		return(temp + 0x30);
	}
	else
	{
		return(temp + 0x37);
	}
}

void strToLCD(char str[])
{
	uint16_t i = 0;
	uint16_t data;
	
	while(str[i])
	{
		data = str[i];
		
		dataToLCD(data);
		i = i + 1;
	}
}

void reg_out(uint32_t reg_data)
{
	int i;
	uint32_t shifted_val;
	uint8_t print_val;
	
	dataToLCD(0x30);
	dataToLCD(0x78);
	
	for(i=28; i>=0; i=(i-4))
	{
		shifted_val = (reg_data >> i) & 0x0F;
		print_val = (convertToASCII(shifted_val));
		dataToLCD(print_val);
	}
	
	dataToLCD(0x20);
}
