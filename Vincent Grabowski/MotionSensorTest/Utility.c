/******************************
*	Utility.c
*
*	Vincent Grabowski
*
*******************************/

#include "stm32f10x.h"
#include "Utility.h"
#include "Clocks.h"


uint16_t Read_SW(void)
{
	uint16_t SW_VAL;
	
	SW_VAL = (((GPIOA->IDR & (GPIO_IDR_IDR6))>>3) \
						| ((GPIOA->IDR & (GPIO_IDR_IDR7))>>5) \
					  | ((GPIOC->IDR & (GPIO_IDR_IDR10))>>9) \
						| ((GPIOC->IDR & (GPIO_IDR_IDR11))>>11)) \
						& 0x000F;
	
	return SW_VAL;
	}

uint16_t Read_PB(void)
{ 
	uint16_t PB_VAL;
	
	PB_VAL = ((( GPIOB->IDR & ( GPIO_IDR_IDR8 )) >> 8 ) \
	| (( GPIOB->IDR & ( GPIO_IDR_IDR9 )) >> 8 ) \
	| (( GPIOC->IDR & ( GPIO_IDR_IDR12 )) >> 10 ) \
	| (( GPIOA->IDR & ( GPIO_IDR_IDR5 )) >> 2 )) \
	& 0x0F;
	
	return PB_VAL;
}

void set_led(uint16_t led_VAL)
{
	uint16_t temp_odr ;
	temp_odr = (GPIOA->ODR &0xE1FF) ;
	temp_odr = (led_VAL << 9 & 0x1E00) ;
	GPIOA->ODR = temp_odr ;
}

uint16_t MS_DL_VAL(void)
{
	uint16_t DL_VAL;
	DL_VAL = ((GPIOB->IDR & (GPIO_IDR_IDR7)) >> 7) & 0x01;
	return DL_VAL;
}
void set_SerinBit(uint16_t bit_VAL)
{
	GPIOB->ODR |= 0x0040;
	delay(2);
	uint16_t temp_odr ;
	temp_odr = (GPIOB->ODR & 0xFFBF);
	temp_odr = (bit_VAL << 6 & 0x0040);
	GPIOB->ODR = temp_odr;
	delay(600);
	GPIOB->ODR &= 0xFFBF;
	delay(2);
	
}
void set_Serin(uint32_t reg_Bits)
{
	uint16_t tempBit;
	for(int16_t i = 24; i >= 0; i--)
		{
			tempBit = ((reg_Bits >> i) & 0x01);
			set_SerinBit(tempBit);
		}
	delay(5000);
}

void EXTI0_init(void)
{
	//Turn on clock for AFIO pins
	RCC->AHBENR |= RCC_APB2ENR_AFIOEN ;
	//Port A pins will be the source for EXTI0
	AFIO->EXTICR[0] &= ~AFIO_EXTICR1_EXTI0 ;
	//unmask PA0 as a source
	EXTI->IMR |= EXTI_IMR_MR0 ;
	//use falling edge of PA0 (blue push button) as trigger
	EXTI->FTSR |= EXTI_FTSR_TR0 ;
	//unmask EXIT as a source in the NVIC
	NVIC->ISER[0] |= NVIC_ISER_SETENA_6 ;
}

void ADC_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN |RCC_APB2ENR_ADC1EN | RCC_APB2ENR_IOPAEN;
	
	GPIOA->CRL &= ~GPIO_CRL_MODE1 & ~GPIO_CRL_MODE2;
	GPIOA->CRL &= ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF2;
	
	ADC1->CR2 = 0x01;
	
}

uint32_t ADC_read(uint32_t channel)
{
	ADC1->SQR3 = channel;
	
	ADC1->CR2 = 0x01;
	
	uint32_t test;
	
	while(test != 0x02)
	{
		test = ADC1->SR;
		test = test & 0x02;
	}
	uint32_t dVAL = ADC1->DR;
	return dVAL;
}

uint32_t convertTomV(uint32_t reg_val)
{
	return reg_val*0.8;
}
	
uint32_t convertoTempC(uint32_t reg_val)
{
	return (reg_val*0.8)/10;
}
	

void USART_Tx(uint8_t data)
{
	
	USART3->CR1 |= USART_CR1_TE;
	uint32_t SR = USART3->SR;
	USART3->DR = data;
	SR = USART3->SR;
	while((USART3->SR & 0x40) == 0)
	{
		SR = USART3->SR;
	}
	
	
}

uint8_t USART_Rx(void)
{
	uint8_t data;
	
	data = USART3->DR;
	//USART3->SR &= ~USART_SR_RXNE;
}

uint32_t distanceTest()
{
	GPIOB->ODR |= 0x400;
	delay(60);
	GPIOB->ODR &= ~0x400;
	
	uint32_t count = 0x00;
	while((GPIOB->IDR & GPIO_IDR_IDR11) == 0)
	{}
		while(( GPIOB->IDR & GPIO_IDR_IDR11)>0)
		{
			delay(0);
			count++;
		}
	
		uint32_t distance = (count/58);
	
	return distance;
	
}