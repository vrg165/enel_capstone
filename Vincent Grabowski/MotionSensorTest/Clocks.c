/******************************************
*
*Clock_init.c
*
*Vincent Grabowski
*
*
******************************************/

#include "stm32f10x.h"
#include "Clocks.h"


void ClockInit(void)
{
	uint32_t temp = 0x00;
	
	RCC->CFGR = 0x00050002; //PLLMUL X3, PREDIV1 is PLL input
	
	RCC->CR = 0x01010081;
	
	while (temp != 0x02000000)  // Wait for the PLL to stabilize
    {
        temp = RCC->CR & 0x02000000; //Check to see if the PLL lock bit is set
    }
	
}

//* Name:         void delay()
//* Paramaters:   32 bit delay value, ( a value of 6000
//                gives approximately 1 mS of delay)
//* Description:  This function creates a delay
//*
void delay(uint32_t count)
{
    int i=0;
    for(i=0; i< count; ++i)
    {
    }
}

void led_IO_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN;
	
	//Set config and mode bits for Port C bit 9 and 8 to push/pull outputs(50MHz)
	GPIOC->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE8 ;
  GPIOC->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF8 ;
	
	//Set config and mode bits for Port a bits 12,11,10,9 to push/pull outputs(50MHz)
	GPIOA->CRH |= GPIO_CRH_MODE12 | GPIO_CRH_MODE11 | GPIO_CRH_MODE10 | GPIO_CRH_MODE9;
	GPIOA->CRH &= ~GPIO_CRH_CNF12 & ~GPIO_CRH_CNF11 & ~GPIO_CRH_CNF10 & ~GPIO_CRH_CNF9;
	
	//uint32_t temp = (GPIOA->CRH & 0xFFF0000F);
	//temp |= 0x00033330;
	//GPIOA->CRH = temp;
	
}

void PYQ_IO_Init(void)
{
	//RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN ;
	
	//Set config and mode bits for Port B bit 6 and Port A pin 15 to push/pull output(50MHz)
	GPIOB->CRL |= GPIO_CRL_MODE6;
	GPIOB->CRL &= ~GPIO_CRL_CNF6;
	
	GPIOB->CRH |= GPIO_CRH_MODE10;
	GPIOB->CRH &= ~GPIO_CRH_CNF10;
	
	//Set config and mode nits for Port B pin 7 to input
	GPIOB->CRL &= ~GPIO_CRL_MODE7;
	GPIOB->CRL &= ~GPIO_CRL_CNF7_1;
	GPIOB->CRL |= GPIO_CRL_CNF7_0;
}

void PB_IO_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN ;
	
	
}	

void STM_IO_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPAEN ;
	
	//Set config and mode bits for Port C bit 9 and 8 to push/pull outputs(50MHz)
	GPIOC->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE8 ;
  GPIOC->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF8 ;
}

void sysTick_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; //Enable Alternative Function for SysTick
	
	SysTick->CTRL = 0x0;
	SysTick->LOAD = 0xB17B00;
	SysTick->VAL = 0x00;
	SysTick->CTRL = 0x07; //24MHz ; exception request enabled ; counter enabled
	
}

void TIM1_init(void)
{
	//Turn on clock for TIM1
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN | RCC_APB2ENR_IOPAEN;
	//Turn on clock for AFIO pins
	RCC->AHBENR |= RCC_APB2ENR_AFIOEN ;
	
	//Set PA9 & PA10 to Alternative Function Output Push-Pull (1011)
	GPIOA->CRH |= GPIO_CRH_MODE9 ;
  GPIOA->CRH &= ~GPIO_CRH_CNF9_0 ;
	GPIOA->CRH |= GPIO_CRH_CNF9_1;
	
	GPIOA->CRH |= GPIO_CRH_MODE10 ;
  GPIOA->CRH &= ~GPIO_CRH_CNF10_0 ;
	GPIOA->CRH |= GPIO_CRH_CNF10_1;
	
	GPIOA->CRH |= GPIO_CRH_MODE8 ;
  GPIOA->CRH &= ~GPIO_CRH_CNF8_0 ;
	GPIOA->CRH |= GPIO_CRH_CNF8_1;
	
	
	TIM1->CR1 |= TIM_CR1_CEN; 	//Enable Timer 1
	TIM1->CR2 |= TIM_CR2_OIS1 | TIM_CR2_OIS2 | TIM_CR2_OIS3;	//Output idle state for CH1 OC1=1 when MOE=0
	TIM1->EGR |= TIM_EGR_UG; 		//reinitialize the counter
	TIM1->CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE | TIM_CCMR1_OC1FE;
	TIM1->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE | TIM_CCMR1_OC2FE;
	TIM1->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE | TIM_CCMR2_OC3FE;
	//PWM mode 1, Preload Enabled, Fast Enable
	
	
	TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E;	//enable CH1 output on PA8, CH2 PA9 & CH3 PA10
	TIM1->PSC = 0x017;		//Divide 24MHz by 24, 1 count = 0.10us
	TIM1->ARR = 10000; 			//10000 counts = 10ms period
	TIM1->CCR1 = 5000;
	TIM1->CCR2 = 5000;			//50000 counts = 5ms, 50% duty cycle
	TIM1->CCR3 = 5000;
	TIM1->BDTR |= TIM_BDTR_MOE | TIM_BDTR_OSSI;	//Main output enable, Force Idle Level First
	TIM1->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; 	//ENABLE Timer 1
}

void TIM1_DC_update(uint16_t count)
{
	TIM1->CCR1 = count;
	TIM1->CCR2 = count;
	TIM1->CCR3 = count;
	TIM1->EGR |= TIM_EGR_UG; 		//reinitialize the counter
}

void USART_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	RCC->AHBENR |= RCC_APB2ENR_AFIOEN ;
	RCC->APB1ENR |= RCC_APB1ENR_USART3EN;
	
	//set PB10 as alternative function output push-pull
	GPIOB->CRH |= GPIO_CRH_MODE10 ;
  GPIOB->CRH &= ~GPIO_CRH_CNF10_0 ;
	GPIOB->CRH |= GPIO_CRH_CNF10_1;
	
	//set PB11 as alternative function output push-pull
	GPIOB->CRH |= GPIO_CRH_MODE11 ;
  GPIOB->CRH &= ~GPIO_CRH_CNF11_0 ;
	GPIOB->CRH |= GPIO_CRH_CNF11_1;
	
	//USART and Tx Rx Enabled
	USART3->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	//set baud rate 9600 bits per second
	USART3->BRR |= 0x09C4;
	
	USART3->CR1 |= USART_CR1_TE | USART_CR1_RE;
}

