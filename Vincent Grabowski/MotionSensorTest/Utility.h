/***********************************
*	Utility.h
*
*	Vincent Grabowski
*
************************************/

#include "stm32f10x.h"
#include <stdint.h>


uint16_t Read_SW(void);

uint16_t Read_PB(void);

void set_led(uint16_t led_VAL);

void set_Serin(uint32_t reg_Bits);
void set_SerinBit(uint16_t bit_VAL);
uint16_t MS_DL_VAL(void);

void EXTI0_init(void);

void ADC_init(void);

uint32_t ADC_read(uint32_t channel);

uint32_t convertTomV(uint32_t reg_val);

uint32_t convertoTempC(uint32_t reg_val);

void USART_Tx(uint8_t data);
	
uint8_t USART_Rx(void);

uint32_t distanceTest();