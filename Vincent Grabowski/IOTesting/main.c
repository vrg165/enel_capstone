//
//	ENEL Capstone Project IO Test
//
//	Vincent Grabowski, March 2020
//
//************************************

#include "stm32f10x.h"
#include <stdint.h>
#include "Clocks.h"
#include "Utility.h"
#include "LCD_utility.h"

int main()
{
	ClockInit();
	PB_IO_init();
	led_IO_init();
	LCD_init();
	LCD_IO_init();
	
	
	commandToLCD(LCD_LN1);
	strToLCD("ENEL400 IO Test");
	set_led(0xF);
	delay(500000);
	//commandToLCD(LCD_LN2);
	//strToLCD("Please_Work");
	uint16_t PB_VAL;
	uint32_t temp_odr;
	
	while(1)
	{
		
		if(PB_VAL != Read_PB())
		{
			PB_VAL = Read_PB();
			set_led(0xF);
			commandToLCD(LCD_CLR);
			commandToLCD(LCD_LN1);
			strToLCD("ENEL400 IO Test");
			
			//Green PB pressed 
			if(PB_VAL == 0x07)
			{	
				set_led(0x7);
				commandToLCD(LCD_CLR);
				commandToLCD(LCD_LN1);
				strToLCD("Temp: 36.7C");
				commandToLCD(LCD_LN2);
				strToLCD("SPO2: 98%");
			}
			//BLUE PB pressed 
			if(PB_VAL == 0x0B)
			{	
				set_led(0xB);
				commandToLCD(LCD_CLR);
				commandToLCD(LCD_LN1);
				strToLCD("Temp: 37.7C");
				commandToLCD(LCD_LN2);
				strToLCD("SPO2: 94%");
			}
			//BLACK PB pressed 
			if(PB_VAL == 0x0D)
			{	
				set_led(0xD);
				commandToLCD(LCD_CLR);
				commandToLCD(LCD_LN1);
				strToLCD("Temp: 39.7C");
				commandToLCD(LCD_LN2);
				strToLCD("SPO2: 80%");
			}
			
			
		}
	}
	
}	