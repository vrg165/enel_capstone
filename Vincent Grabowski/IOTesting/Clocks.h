/***************************************
*Clock_init.h 
*
*Vincent Grabowski
*
*
****************************************/

#include "stm32f10x.h"
#include <stdint.h>

//Initialize Cortex M3 clock
void ClockInit(void);

void delay(uint32_t delay);

void led_IO_init(void);

void PB_IO_init(void);

void STM_IO_init(void);

void sysTick_init(void);

void TIM1_init(void);

void TIM1_DC_update(uint16_t count);

void USART_init(void);

