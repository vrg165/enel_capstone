
#ifndef CLOCKS_H
#define CLOCKS_H


#include "includeList.h"

#define SysTickLoad 0xE4E1C0

void clock_init(void);
void delay(uint32_t count);
void sysTick_init(void);

#endif
