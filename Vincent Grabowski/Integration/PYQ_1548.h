
#ifndef PYQ_1548_H
#define PYQ_1548_H

#include "includeList.h"

#define PYQ_REG_BITS 0x0020E931

void PYQ_1548_init(void);
void set_SerinBit(uint16_t bit_VAL);
void set_Serin(uint32_t reg_Bits);
uint16_t PYQ_DL_VAL(void);
void resetPYQ(void);
	
#endif
