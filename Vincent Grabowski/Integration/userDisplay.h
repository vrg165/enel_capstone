
#ifndef USERDISPLAY_H
#define USERDISPLAY_H

#include <stdint.h>
#include "includeList.h"

//Commands for Hitachi 44780 compatible LCD controllers

#define LCD_8B2L 0x38 // ; Enable 8 bit data, 2 display lines
#define LCD_DCB 0x0F // ; Enable Display, Cursor, Blink
#define LCD_MCR 0x06 // ; Set Move Cursor Right
#define LCD_CLR 0x01 // ; Home and clear LCD
#define LCD_LN1 0x80 // ;Set DDRAM to start of line 1
#define LCD_LN2 0xC0 // ; Set DDRAM to start of line 2

// Control signal manipulation for LCDs on 352/384/387 board
// PB0:RS PB1:ENA PB5:R/W*

#define LCD_CM_ENA 0x00210002 //
#define LCD_CM_DIS 0x00230000 //
#define LCD_DM_ENA 0x00200003 //
#define LCD_DM_DIS 0x00220001 //


void commandToLCD(uint8_t data);
void dataToLCD(uint8_t data);
void display32int(uint32_t);
void LCD_init(void);
void LCD_IO_init(void);
uint8_t convertToASCII(uint16_t data);
void strToLCD(char[]);
void reg_out(uint32_t reg_data);

void LCD_Welcome(void);
void printGood(uint8_t SPO2, float TEMP);
void printError(uint8_t SPO2, float TEMP);
void printBad(uint8_t SPO2, float TEMP);

void GPIO_PIN_init(void);

#define BAD_SPO2_READ 	0x0050
#define LOW_SPO2 				0x005D
#define BAD_TEMP_READ 	27
#define HIGH_TEMP 			33

uint8_t calcAvgSPO2(uint8_t *SPO2);
float calcAvgTEMP(float *TEMP);

#endif
