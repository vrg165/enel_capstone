#include "usart.h"

void USART_init()
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN;
	RCC->AHBENR |= RCC_APB2ENR_AFIOEN ;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	
	RCC->APB1ENR |= RCC_APB1ENR_USART3EN | RCC_APB1ENR_USART2EN;
	
	//set PB10 as alternative function output push-pull
	GPIOB->CRH |= GPIO_CRH_MODE10 | GPIO_CRH_CNF10_1;
  GPIOB->CRH &= ~GPIO_CRH_CNF10_0 ;
	
	GPIOA->CRL |= GPIO_CRL_MODE2 | GPIO_CRL_CNF2_1;
	GPIOA->CRL &= ~GPIO_CRL_CNF2_0;
	
	//set PB11 as alternative function output push-pull
	GPIOB->CRH |= GPIO_CRH_MODE11 | GPIO_CRH_CNF11_1;
  GPIOB->CRH &= ~GPIO_CRH_CNF11_0 ;
	
	GPIOA->CRL |= GPIO_CRL_MODE3 | GPIO_CRL_CNF3_1;
	GPIOA->CRL &= ~GPIO_CRL_CNF3_0;
	
	//USART and Tx Rx Enabled
	USART3->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	//set baud rate 9600 bits per second
	USART3->BRR |= 0x09C4;
	USART2->BRR |= 0x09C4;
	USART3->CR1 |= USART_CR1_TE | USART_CR1_RE;
	USART2->CR1 |= USART_CR1_TE | USART_CR1_RE;
	
}

void USART_TX_Byte(USART_TypeDef* USARTx, uint8_t data)
{
	USARTx->CR1 |= USART_CR1_TE;
	volatile uint32_t SR = USARTx->SR;
	USARTx->DR = data;
	SR = USARTx->SR;
	while((USARTx->SR & 0x40) == 0){}
}
uint8_t USART_RX_Byte(USART_TypeDef* USARTx)
{
	volatile uint8_t data;
	data = USARTx->DR;
	while((USARTx->SR & 0x20) != 0){}
	return data;
}
void USART_RX_User_Data(USART_TypeDef* USARTx, uint8_t *data)
{
	volatile uint8_t i = 0;
	while(i < 5)
	{
		if((USARTx->SR & 0x20) == 0x20)
		{
			data[i] = USART_RX_Byte(USARTx);
			//USART_TX_Byte(USARTx, 0x63);
			//delay(600);
			//data[i] = USART_RX_Byte(USARTx);
			i++;
		}
	}
}

void get_10_Measure(USART_TypeDef* USARTx, uint8_t *SPO2, float *TEMP)
{
	volatile int i = 0;
	uint8_t data[5];
	char temp[4];
	
	while(i < 10)
	{
		if((USARTx->SR & 0x20) == 0x20)
		{
			USART_RX_User_Data(USARTx, data);
			SPO2[i] = data[0];
			
			temp[0] = data[1];
			temp[1] = data[2];
			temp[2] = data[3];
			temp[3] = data[4];
			sscanf(temp, "%f", &TEMP[i]);
			
			
			
			//TEMP[i] = strtof(temp);
			//temp = 0;
			//temp |= (data[1] <<24) | (data[2] << 16) | (data[3] << 8) | data[4];
			//TEMP[i] = temp;
			i++;
		}
	}
}
