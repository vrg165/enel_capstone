
#include "PYQ_1548.h"

//	Initializes required pins for Motion Sensor
//	Turn on clock for Port B GPIO pins
//	Sets Pins PB6, PB7, PB8 
void PYQ_1548_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	
	//Set config and mode bits for Port B bit 6 and Port B pin 8 to push/pull output(50MHz)
	GPIOB->CRL |= GPIO_CRL_MODE6;
	GPIOB->CRL &= ~GPIO_CRL_CNF6;
	
	//****************************
	//Remember to turn back to pin 8 Below
	//***\/***\/***\/***
	GPIOB->CRH |= GPIO_CRH_MODE8;
	GPIOB->CRH &= ~GPIO_CRH_CNF8;
	
	//Set config and mode nits for Port B pin 7 to input
	GPIOB->CRL &= ~GPIO_CRL_MODE7;
	GPIOB->CRL &= ~GPIO_CRL_CNF7_1;
	GPIOB->CRL |= GPIO_CRL_CNF7_0;
}

void set_SerinBit(uint16_t bit_VAL)
{
	GPIOB->ODR |= 0x0040;
	delay(2);
	uint16_t temp_odr ;
	temp_odr = (GPIOB->ODR & 0xFFBF);
	temp_odr = (bit_VAL << 6 & 0x0040);
	GPIOB->ODR = temp_odr;
	delay(600);
	GPIOB->ODR &= 0xFFBF;
	delay(2);
	
}

void set_Serin(uint32_t reg_Bits)
{
	uint16_t tempBit;
	for(int16_t i = 24; i >= 0; i--)
		{
			tempBit = ((reg_Bits >> i) & 0x01);
			set_SerinBit(tempBit);
		}
	delay(5000);
}

uint16_t PYQ_DL_VAL(void)
{
	uint16_t DL_VAL;
	DL_VAL = ((GPIOB->IDR & (GPIO_IDR_IDR7)) >> 7) & 0x01;
	return DL_VAL;
}

void resetPYQ(void)
{
	GPIOB->ODR |= GPIO_ODR_ODR8;
	delay(600);
	GPIOB->ODR &= ~GPIO_ODR_ODR8;
}