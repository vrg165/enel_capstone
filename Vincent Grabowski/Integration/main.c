//
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	Version: 1.0
//	March 2021
//
//	Vincent Grabowski
//
//********************************************************************

#include "includeList.h"

char state = 'A';
uint8_t sleepCount = 0;
uint8_t userData[5];
uint8_t userSPO2[10];
uint8_t avgSPO2 = 0;
float userTEMP[10];
float avgTEMP = 0;
uint8_t dataCount = 0;
char dis0[32];

int main()
{
	clock_init();
	sysTick_init();
	LCD_IO_init();
	USART_init();
	GPIO_PIN_init();
	PYQ_1548_init();
	set_Serin(PYQ_REG_BITS);
	uint8_t tempFix;

	while(1)
	{		
		if(state == 'A')
		{
//			tempFix = USART_RX_Byte(USART2);
//			tempFix = USART_RX_Byte(USART2);
//			tempFix = USART_RX_Byte(USART2);
//			tempFix = USART_RX_Byte(USART2);
//			tempFix = USART_RX_Byte(USART2);
			
			//GPIOA->ODR &= ~GPIO_ODR_ODR11;
			dataCount = 0;
			LCD_Welcome();
			GPIOA->ODR |= GPIO_ODR_ODR1;

			while(state == 'A')
			{
				if((USART2->SR & 0x20) == 0x20)
				{
					USART_RX_User_Data(USART2, userData);
					if(userData[0] >= 60)
						dataCount++;
					else 
						dataCount = 0;
				}
				
				//Take next 10 measurements from usart and average value
				if(dataCount >=3)
				{
					get_10_Measure(USART2, userSPO2, userTEMP);
					dataCount = 0;
					state = 'B';
				}
			}
			GPIOA->ODR &= ~GPIO_ODR_ODR1;
		}
		//User Display State
		//user data stored in userSPO2[] and userTEMP[]
		if(state == 'B')
		{
			avgTEMP = calcAvgTEMP(userTEMP);
			avgSPO2 = calcAvgSPO2(userSPO2);
			if(avgTEMP < BAD_TEMP_READ || avgSPO2 < BAD_SPO2_READ){
				printError(avgSPO2, avgTEMP);
				GPIOA->ODR &= ~GPIO_ODR_ODR8;
				state = 'A';
			}
			else if(avgTEMP > HIGH_TEMP || avgSPO2 < LOW_SPO2){
				printBad(avgSPO2, avgTEMP);
				state = 'C';
			}
			else{
				printGood(avgSPO2, avgTEMP);
				state = 'C';
			}
			sleepCount = 0;
			SysTick->VAL = SysTickLoad;
			while(sleepCount < 1){}
			GPIOA->ODR |= GPIO_ODR_ODR10 | GPIO_ODR_ODR11 | GPIO_ODR_ODR12;
		}
		if(state == 'C')
		{
			USART_TX_Byte(USART3, avgSPO2);
			sprintf(dis0,"%4.1f", avgTEMP);
			USART_TX_Byte(USART3, dis0[0]);
			USART_TX_Byte(USART3, dis0[1]);
			USART_TX_Byte(USART3, dis0[2]);
			USART_TX_Byte(USART3, dis0[3]);
			
			commandToLCD(LCD_CLR);
			strToLCD(" Disinfecting ");
			commandToLCD(LCD_LN2);
			strToLCD(" Do not Scan ");
			
			GPIOA->ODR |= GPIO_ODR_ODR8;
			sleepCount = 0;
			SysTick->VAL = SysTickLoad;
			while(sleepCount < 2){}//10 seconds
			
			GPIOA->ODR &= ~GPIO_ODR_ODR8;
			state = 'A';
		}
		
		if(state == 'S')
		{
			GPIOA->ODR &= ~GPIO_ODR_ODR0 & ~GPIO_ODR_ODR9;
			commandToLCD(LCD_CLR);
			strToLCD("SLEEP MODE");
			while(state == 'S'){
				if(PYQ_DL_VAL() == 0x01){
					resetPYQ();
					sleepCount = 0;
					SysTick->VAL = SysTickLoad;
					GPIOA->ODR |= GPIO_ODR_ODR9;
					state = 'A';
				}
			}
		}
	}
}	

void SysTick_Handler(void)
{
	sleepCount++;
	if(sleepCount >= 24)
	{
		if(PYQ_DL_VAL() == 0x01){
			SysTick->VAL = SysTickLoad;
			resetPYQ();
		}
		else
			state = 'S';
		sleepCount = 0;
	}
}
	