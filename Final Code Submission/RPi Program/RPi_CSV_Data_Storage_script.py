#Created by Thomas Martineau
#Takes data from STM32F100RB and stores in CSV file on Pi
#Uses USART on PI
import serial
import time
import datetime
import csv

#Opens up the serial port with a baud rate of 9600
serialport = serial.Serial()
serialport.baudrate = 9600
serialport.bytesize = serial.EIGHTBITS
serialport.port = "/dev/serial0"
serialport.stopbits=serial.STOPBITS_ONE
serialport.parity = serial.PARITY_EVEN
serialport.open()

temp = 0
sp02 = 0
users = 0

day = time.strftime("%Y%m%d")
print(day)
newDay = 0;

while True:
	#Check for inputs.
	input = serialport.read()
	#Get current time/date
	day = time.strftime("%Y%m%d")
	
	#if there is an input, do stuff
	if input!=None:
		#opens file with current day in append mode
		csv = open(day + ".csv","a")
		sp02 = ord(input)
		temp = serialport.read(4)
		#add to user count
		users = users+1
		#get current time
		daytime = time.asctime(time.localtime(time.time()))
		#print current user info to terminal
		print('New User -> Temperature: {}. SP02 {}. Total Users {}. Date/Time {}'.format(temp,sp02,users,daytime))
		#store current data to CSV file
		csv.write( str(sp02) + "," + str(temp) + "," + str(users) + "," + daytime + "\n")
		csv.close()
