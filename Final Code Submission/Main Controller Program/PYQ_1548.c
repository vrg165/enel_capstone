//
//	Functions Used for PYQ1548 Motion Sensor
//	March 2021
//	Vincent Grabowski
//
#include "PYQ_1548.h"

//	March 2021
//	Vincent Grabowki
//
//	Initializes required pins for Motion Sensor
//	Turn on clock for Port B GPIO pins
//	Sets Pins PB6, PB7, PB8 
void PYQ_1548_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
	
	//Set config and mode bits for Port B bit 6 and Port B pin 8 to push/pull output(50MHz)
	GPIOB->CRL |= GPIO_CRL_MODE6;
	GPIOB->CRL &= ~GPIO_CRL_CNF6;
	GPIOB->CRH |= GPIO_CRH_MODE8;
	GPIOB->CRH &= ~GPIO_CRH_CNF8;
	
	//Set config and mode bits for Port B pin 7 to input
	GPIOB->CRL &= ~GPIO_CRL_MODE7;
	GPIOB->CRL &= ~GPIO_CRL_CNF7_1;
	GPIOB->CRL |= GPIO_CRL_CNF7_0;
}

//
//	Feb 2021
//	Vincent Grabowski
//
//	Function sends a single bit to the PYQ SERIN pin
//	Must set PB6 high briefly then hold high or low for
//	at least 72micro-seconds before going low again.
//	Bit sent to PYQ register equals bit 0 of bit_VAL
//
//	Function utilizes delay() function defined in clocks.h
void set_SerinBit(uint16_t bit_VAL)
{
	GPIOB->ODR |= 0x0040;
	delay(2);
	uint16_t temp_odr ;
	temp_odr = (GPIOB->ODR & 0xFFBF);
	temp_odr = (bit_VAL << 6 & 0x0040);
	GPIOB->ODR = temp_odr;
	delay(600); //hold bit value for approximately 100 micro-seconds
	GPIOB->ODR &= 0xFFBF;
	delay(2);
	
}
//
//	Feb 2021
//	Vincent Grabowski
//
//	Function called to set all 25 bits of PYQ register
//	via the SERIN pin connected to PB6. 
//	Takes bits 24-0 of reg_Bits as an input and uses 
//	set_SerinBit() function to program all 25 bits 
void set_Serin(uint32_t reg_Bits)
{
	uint16_t tempBit;
	for(int16_t i = 24; i >= 0; i--)
		{
			tempBit = ((reg_Bits >> i) & 0x01);
			set_SerinBit(tempBit);
		}
	delay(5000);
}

//
//	March 2021
//	Vincent Grabowski
//
//	Function reads PYQ DL pin connected to PB7
//	returns pin value as 0x01 for high or 0x00 for low
uint16_t PYQ_DL_VAL(void)
{
	uint16_t DL_VAL;
	DL_VAL = ((GPIOB->IDR & (GPIO_IDR_IDR7)) >> 7) & 0x01;
	return DL_VAL;
}
//
//	March 2021
//	Vincent Grabowski
//
//	In order to reset PYQ after motion has been detected and
//	PB7 goes high, PB7 must temporarily be pulled down to GND
//	Function sets pin PB8 high which opens a transistor that
// 	pulls PB7 down to GND, reseting sensor.
void resetPYQ(void)
{
	GPIOB->ODR |= GPIO_ODR_ODR8;
	delay(600);
	GPIOB->ODR &= ~GPIO_ODR_ODR8;
}