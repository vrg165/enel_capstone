//
//	Feb 2021
//	Vincent Grabowski
//
//	Functions written for Integration regarding clocks
//	delay() function given in ENEL 387 Lab 2020


#include "clocks.h"

//	Feb 2021
//	Vincent Grabowski
//
//	This Function initializes the main clock
void clock_init(void)
{
	uint32_t temp = 0x00;
	
	RCC->CFGR = 0x00050002; //PLLMUL X3, PREDIV1 is PLL input
	
	RCC->CR = 0x01010081;
	
	while (temp != 0x02000000)  // Wait for the PLL to stabilize
    {
        temp = RCC->CR & 0x02000000; //Check to see if the PLL lock bit is set
    }
	
}

//* Name:         void delay()
//* Paramaters:   32 bit delay value, ( a value of 6000
//                gives approximately 1 mS of delay)
//* Description:  This function creates a delay
//*
void delay(uint32_t count)
{
    int i=0;
    for(i=0; i< count; ++i)
    {
    }
}

//
//	March 2021
//	Vincent Grabowski
//
//	This function initializes the sysTick - counter enabled, 3MHz
void sysTick_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN; //Enable Alternative Function for SysTick
	
	SysTick->CTRL = 0x0;
	SysTick->LOAD = SysTickLoad;
	SysTick->VAL = 0x00;
	SysTick->CTRL = 0x03; //3MHz ; exception request enabled ; counter enabled
	
}
