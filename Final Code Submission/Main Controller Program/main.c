//
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	Version: 2.1
//	March 2021
//
//	Vincent Grabowski
//
//	This Program is run on the main STM32F100RB microcontroller. 
//	Controls PYQ_1548 Motion sensor, User Display, the User-State-Cycle
//	along with user measurement data management.
//
//	Communication with the secondary STM32 and the Raspberry Pi is 
//	done via USART 2 and 3 Respectively.
//
//************************************************************************

#include "includeList.h"

char state = 'A';				//used for state control 
uint8_t sleepCount = 0;	//increments every 5 seconds
uint8_t userData[5];		//stores 1 measurement - SPO2 and TEMP
uint8_t userSPO2[10];		//stores 10 SPO2 measurements for avg calculation
uint8_t avgSPO2 = 0;		
float userTEMP[10];			//stores 10 TEMP measurements for avg calculation
float avgTEMP = 0;
uint8_t dataCount = 0;	//keeps track of SPO2 measurements to determine when user
												//data is being recieved
char dis0[32];					//string of char used for sending values to display

int main()
{
	clock_init();
	sysTick_init();
	LCD_IO_init();
	USART_init();
	GPIO_PIN_init();
	PYQ_1548_init();
	set_Serin(PYQ_REG_BITS);
	uint8_t tempFix;

	while(1)
	{	
//***********************************************************************		
//	READY STATE		(state = 'A')
//
//	Displays welcome message on LCD screen and Allows second STM to
//	transmit user measurements. (PA1 = 1)
//
		if(state == 'A')
		{
			dataCount = 0;
			LCD_Welcome();
			GPIOA->ODR |= GPIO_ODR_ODR1;
			
			//	While in READY STATE recieve data on USART every 100ms.
			//	When we receive 3 SPO2 measurements > 60 in a row, that means 
			//	a user finger is on the sensor and we can record their measurements
			//
			while(state == 'A')
			{
				if((USART2->SR & 0x20) == 0x20)
				{
					USART_RX_User_Data(USART2, userData);
					if(userData[0] >= 60)
						dataCount++;
					else 
						dataCount = 0;
				}
				//	Once we are receiving good data: (dataCount >= 3)
				//	Take next 10 measurements from usart and calc average value.
				//	Proceed to Display State (state = 'B')
				if(dataCount >=3)
				{
					get_10_Measure(USART2, userSPO2, userTEMP);
					dataCount = 0;
					state = 'B';
				}
			}
			//	Stop allowing user data transmission when leaving ready state
			GPIOA->ODR &= ~GPIO_ODR_ODR1;
		}
		
//***********************************************************************		
//	DISPLAY STATE		(state = 'B')
//
//	From the 10 measurements recorded, Calculate average SPO2 and TEMP.
//	store values in avgTEMP and avgSPO2.
//	Compare Average values to thresholds defined in userDisplay.h to 
//	to determine result type ("good", "bad", or "error").
//	Print results to User, wait 5 seconds using SYSTICK, proceed to 
//	Ready state if error result, otherwise sanitize state (state = 'C').
//
		if(state == 'B')
		{
			avgTEMP = calcAvgTEMP(userTEMP);
			avgSPO2 = calcAvgSPO2(userSPO2);
			if(avgTEMP < BAD_TEMP_READ || avgSPO2 < BAD_SPO2_READ){
				printError(avgSPO2, avgTEMP);
				GPIOA->ODR &= ~GPIO_ODR_ODR8;
				state = 'A';
			}
			else if(avgTEMP > HIGH_TEMP || avgSPO2 < LOW_SPO2){
				printBad(avgSPO2, avgTEMP);
				state = 'C';
			}
			else{
				printGood(avgSPO2, avgTEMP);
				state = 'C';
			}
			sleepCount = 0;
			SysTick->VAL = SysTickLoad;
			while(sleepCount < 1){}//5 seconds
			//turn off all LEDs
			GPIOA->ODR |= GPIO_ODR_ODR10 | GPIO_ODR_ODR11 | GPIO_ODR_ODR12;
		}

//***********************************************************************
//	SANITIZE STATE		(state = 'C')
//	
//	Send user data to Raspberry Pi via USART3.
//	Turn on UVC LED (PA8 = 1) and Print Warning message to users on 
//	LCD screen for 10 seconds using SYSTICK.
//	Return to Ready state (state = 'A')
//
		if(state == 'C')
		{
			USART_TX_Byte(USART3, avgSPO2);
			sprintf(dis0,"%4.1f", avgTEMP);
			USART_TX_Byte(USART3, dis0[0]);
			USART_TX_Byte(USART3, dis0[1]);
			USART_TX_Byte(USART3, dis0[2]);
			USART_TX_Byte(USART3, dis0[3]);
			
			commandToLCD(LCD_CLR);
			strToLCD(" Disinfecting ");
			commandToLCD(LCD_LN2);
			strToLCD(" Do not Scan ");
			
			GPIOA->ODR |= GPIO_ODR_ODR8;
			sleepCount = 0;
			SysTick->VAL = SysTickLoad;
			while(sleepCount < 2){}//10 seconds
			
			GPIOA->ODR &= ~GPIO_ODR_ODR8;
			state = 'A';
		}

//***********************************************************************
//	SLEEP STATE		(state = 'S')
//	
//	When entering sleep state turn off LCD Screen. 
//	While in sleep state check motion sensor DL pin (PB7) for motion.
//	Once motion detected reset PYQ sensor, reset sleep timer, turn on LCD,
//	and return to Ready State (state = 'A')
//		
		if(state == 'S')
		{
			GPIOA->ODR &= ~GPIO_ODR_ODR0 & ~GPIO_ODR_ODR9;
			commandToLCD(LCD_CLR);
			strToLCD("SLEEP MODE");
			while(state == 'S'){
				if(PYQ_DL_VAL() == 0x01){
					resetPYQ();
					sleepCount = 0;
					SysTick->VAL = SysTickLoad;
					GPIOA->ODR |= GPIO_ODR_ODR9;
					state = 'A';
				}
			}
		}
	}
}	
//***********************************************************************	
//	SYSTICK Interrupt occurs every 5 seconds.
// 	Increases sleepCount each interrupt. Once sleepCount = 24
//	2 minutes has passed.
//
//	After every 2 minutes check PYQ DL pin: 
//	if low: no motion detected - go to Sleep State (state = 'S')
//	if high: motion detected - reset PYQ, reset sleepCount
void SysTick_Handler(void)
{
	sleepCount++;
	if(sleepCount >= 24)
	{
		if(PYQ_DL_VAL() == 0x01){
			SysTick->VAL = SysTickLoad;
			resetPYQ();
		}
		else
			state = 'S';
		sleepCount = 0;
	}
}
	