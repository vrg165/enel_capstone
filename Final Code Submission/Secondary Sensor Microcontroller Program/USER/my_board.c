/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//	
//	Delay function defition
//	
******************************************************************************************/
#include "my_include.h"

#ifndef  __RT_THREAD_H__
extern int $Super$$main(void);
int $Sub$$main(void)
{
#if !defined (USE_HAL_DRIVER)
    NVIC_Configuration();
    My_SysTick_Init();

#endif
    delay_init();//initial delay function
#if CODE_ENCRYPR>0
#endif

#if defined(__MY_KEY_H)

#endif
    $Super$$main();
    return 0;
}
void NVIC_Configuration(void)
{
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
}
#else

#endif
