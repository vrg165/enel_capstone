/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang, Thomas Martineau
//
//	Program is designed for STM32F100RB microcontroller. 
//	Utilizes Temperature and SPO2 functions to retrieve data from the MLX90614/MAX30102 modules, 
//	and send to main microcontroller over UART.
//	
//	INPUTS: USART3, I2C, PC13
//	OUTPUTS: USART3, I2C, USART2
//************************************************************************/
#include "my_include.h"
#include "stdlib.h"

char dis0[32];
float readTemp = 0;// initialize temperature setting
uint16_t lcd_back = 0x200;
uint32_t gpiohigh = 0;

int main(void)
{
    LCD1602_Init();
    My_MAX301xx_Init(USART3);
    My_MLX90614_Init();
		Usart2_Init();

		//Initialized PC13 input with pull up/pull down configuration. 
		//PC13 is used to signal when main controllor is ready to receive measurement data. 
		GPIOC->CRH &= ~GPIO_CRH_MODE13;
		GPIOC->CRH &= ~GPIO_CRH_CNF13_0;
		GPIOC->CRH |= GPIO_CRH_CNF13_1; 
	
		
	
     while(1)
    {
				//myReadFlag_tick is triggered every 100ms using SysTick
        if(myReadFlag_tick==true)
        {
            myReadFlag_tick = false;
            readTemp = My_MLX90614_ReadObjectTemp();//read object temperature
            My_MAX301xx_Uart_GetSPO2(USART3);//send command to get data
            sprintf(dis0," Temp:%4.1fC    ",readTemp);//transform data format
            sprintf(dis0," SPO2:%03d ",SPO2_max301xx);//transform data format
					  
					  
						//Checking to see if PC13 is high. If it is, send data. If low do nothing. 
						gpiohigh = ((GPIOC->IDR & (GPIO_IDR_IDR13)) >> 13) & 0x1;
					
						if(gpiohigh > 0x0){
						My_USART_SendByte(USART2, SPO2_max301xx); //send the SP02 info over USART to other MCU
						sprintf(dis0,"%3.1f",readTemp); //Format Temperature measurement into a string to send over USART
						My_USART_SendString(USART2, dis0);	//send the Temp info over USART to other MCU
	
						}
        }
		
		}
	}
