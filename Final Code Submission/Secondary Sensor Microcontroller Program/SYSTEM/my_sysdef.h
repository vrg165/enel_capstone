/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//	define boolean variable for mlx90614 to use
//
//	
******************************************************************************************/

#ifndef __MY_SYSDEF_H
#define __MY_SYSDEF_H

#include "my_board.h"
#include <string.h> 
#include <stdio.h> 
#include <stdarg.h>

/*Boolean Variable*/
typedef enum
{
    false =0,
    true =!false
} bool;

#if defined(USE_RT_THREAD)
#else//USE_RT_THREAD
#endif//USE_RT_THREAD

#endif
