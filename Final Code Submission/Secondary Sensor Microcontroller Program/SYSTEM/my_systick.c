/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//	Zhengyang Zhang
//	systic definitions
//	refrerenced from stm32f10x libiary
//
******************************************************************************************/
#include "my_systick.h"

#define VERSION     11

static u32 sysTicks=0;

void My_SysTick_Init(void)
{
    SysTick_Config(SystemCoreClock / TICK_PER_SECOND);
}
u32 My_SysTick_GetTicks(void)
{
    return sysTicks;
}

#if FLAG_SEND_SYSTICK>0
bool mySendFlag_tick = false;//send data sign
#endif
#if FLAG_READ_SYSTICK>0
bool myReadFlag_tick = false;//read data sign
#endif

#if !defined (USE_HAL_DRIVER)
void SysTick_Handler(void)   //SysTick stoped 
{

#endif
    sysTicks++;
#if FLAG_READ_SYSTICK>0
    if(sysTicks%((int)(100/My_SysTick_GetPeriod()))==0)
    {
        myReadFlag_tick = true;
    }
#endif
#if FLAG_SEND_SYSTICK>0
    if(sysTicks%((int)(500/My_SysTick_GetPeriod()))==1)
    {
        mySendFlag_tick = true;
    }
#endif
}
u16 My_SysTick_GetVersion(void)
{
    return VERSION;
}