/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//
//	This Program is run on the STM32F100RB microcontroller. 
//	This file also connects to the pins on the ENEL 387 board
//	and the definitions
//	
******************************************************************************************/
#ifndef __MY_LCD1602_H
#define __MY_LCD1602_H

#include "my_include.h"


#define PIN_RS          PB0
#define PIN_RW          PB5
#define PIN_EN          PB1

const static MyPinDef Pins_Data_1602[] = {PC0,PC1,PC2,PC3,PC4,PC5,PC6,PC7};


#define SET_EN          PinSet(PIN_EN)		//output 1
#define CLE_EN          PinReset(PIN_EN)	//output 0 
#define SET_RW          PinSet(PIN_RW)    //output 1
#define CLE_RW          PinReset(PIN_RW)	//output 0
#define SET_RS          PinSet(PIN_RS)		//output 1
#define CLR_RS          PinReset(PIN_RS)	//output 0

#define LCD_Puts        LCD_PutStr
#define LCD_1Put        LCD_PutChar


void LCD1602_Init(void);
void LCD1602_SetCursor(u8 x,u8 y);
void LCD1602_CloseCursor(void);
void LCD1602_Clear(void);
void LCD1602_Write_String(u8 x,u8 y,const  char *string);
void LCD1602_Write_Char(u8 x,u8 y,const char disChar);

#endif
