/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//
//	This Program is run on the main STM32F100RB microcontroller. 
//	And it receives the data from the MAX30102 sensor through USART.
//	The data is converted in ASCII code.
//	This program will also filter the bad reading by checking the HEX value.
//	
//
//************************************************************************/

#include "my_max301xx_uart.h"
u8 SPO2_max301xx = 0;

void My_MAX301xx_Init(USART_TypeDef* USARTx)
{
    My_USART_Init(USARTx,9600);
    My_USART_SetReceiveByteHook(USARTx,My_MAX301xx_Uart_StateMachine);
}
void My_MAX301xx_Uart_StateMachine(u8 msgByte)
{
    static u8 state_SPO2=0;
    static u8 index_dat_SPO2=0;
    static u8 SPO2_temp=0;

    if( StateMachine_GetStr("+SPO2=",msgByte,&state_SPO2))
    {
        index_dat_SPO2 = 1;
        SPO2_temp = 0;
        return;
    }

    if(index_dat_SPO2>0)
    {
        if (msgByte==0x0d)
        {
            index_dat_SPO2=0;
            SPO2_max301xx = SPO2_temp;
            return;
        }
        else if (msgByte>=0x30 && msgByte<0x3a)
        {
            SPO2_temp = SPO2_temp*10 + msgByte-0x30;
            index_dat_SPO2++;
        }
    }
}

void My_MAX301xx_Uart_GetSPO2(USART_TypeDef* USARTx)
{
    USARTSendString(USARTx,"AT+SPO2\r\n");
}

