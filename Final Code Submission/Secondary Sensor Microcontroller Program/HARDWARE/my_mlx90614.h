/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//	function headers for mlx90614.c
//
//	
******************************************************************************************/
#ifndef __MY_MLX90614_H
#define __MY_MLX90614_H
#include "my_include.h"

#define My_MLX90614_Init        IIC_Init
float My_MLX90614_ReadObjectTemp(void); 

#endif
