/******************************************************************************
 * Name:    LCD.c
 * Description: Will handle LCD initialization and functions
 * Version: V1.00
 * Author: Thomas Martineau
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "LCD.h"
 #include "Clock.h"
 
 //Inits GPIO B and C as thats what the LCD uses. 
void LCD_IO_Init(void){
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN;
	
	GPIOB->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE5 | GPIO_CRL_MODE1 ;//changing mode to output
	GPIOC->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7;//changing mode to output
	
	GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF1; //changing PB 0, 1, 5 to general purpose output push/pull
	GPIOC->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF2 & ~GPIO_CRL_CNF3 & ~GPIO_CRL_CNF4 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF6 & ~GPIO_CRL_CNF7; //changing PB 0, 1, 5 to general purpose output push/pull
}

void commandToLCD(uint8_t data)
 {
	GPIOB->BSRR = LCD_CM_ENA; //RS low, E high
	
	 // GPIOC->ODR = data; //BAD: may affect upper bits on port C
	
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	
	delay(8000);
	
	GPIOB->BSRR = LCD_CM_DIS; //RS low, E low
	
	delay(80000);
}
 void dataToLCD(uint8_t data) 
{
	GPIOB->BSRR = LCD_DM_ENA; //RS low, E high
	
	 // GPIOC->ODR = data; //BAD: may affect upper bits on port C
	
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	
	delay(8000);
	
	GPIOB->BSRR = LCD_DM_DIS; //RS low, E low
	
	delay(80000);
}

void LCD_INIT(void){
	commandToLCD(LCD_8B2L);
	commandToLCD(LCD_8B2L);
	commandToLCD(LCD_8B2L);
	commandToLCD(LCD_8B2L);
	commandToLCD(LCD_DCB);
	commandToLCD(LCD_CLR);
	commandToLCD(LCD_MCR);
}

uint8_t hex2ascii(uint8_t displayvalue){
	uint8_t asci = 0x00;
		if (displayvalue <= 0x009){	
		asci = 0x30 | displayvalue;
		return asci;
		}
		else
		{
		asci = 0x40 | (displayvalue - 9);
		return asci;
		}
	
}

void outputint32(uint32_t displayvalue){
int i;
uint32_t shifteddisplayvalue;
uint8_t outputvalue;
dataToLCD(0x30);
dataToLCD(0x78);

for ( i=28; i >= 0 ; i = (i-4))
{
shifteddisplayvalue = (displayvalue >> i) & 0xf;
outputvalue = (hex2ascii(shifteddisplayvalue));
dataToLCD(outputvalue);
}
dataToLCD(0x20);
}