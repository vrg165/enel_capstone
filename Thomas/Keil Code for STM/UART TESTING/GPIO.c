/******************************************************************************
 * Name:    GPIO.c
 * Description: Will handle GPIO initialization and functions
 * Version: V1.00
 * Author: Thomas Martineau
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "GPIO.h"
 
 //Inits GPIO A, B, C
 void GPIOInit(void){
	 RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPCEN | RCC_APB2ENR_AFIOEN; 
 }
 
 void discoveryIOInit(void){
	GPIOC->CRH |= GPIO_CRH_MODE8 | GPIO_CRH_MODE9; 
	GPIOC->CRH &= ~GPIO_CRH_CNF8 & ~GPIO_CRH_CNF9;
 }
 
 void LEDInit(void){
	 GPIOA->CRH |= GPIO_CRH_MODE9 | GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12;//changing mode to output
	 GPIOA->CRH &= ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF10 & ~GPIO_CRH_CNF11 & ~GPIO_CRH_CNF12; //changing PA9-12 to general purpose output push/pull
 }
 
 void TIM1CLKInit(void){
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	GPIOA->CRH &= ~GPIO_CRH_CNF8;
	GPIOA->CRH |= 0x0000000B;
 }
 
 //getpushbuttons is a modified version of the example given on URcourses. 
 //I changed what buttons go where in the variable i.e the shifting
 uint16_t getpushbuttons(void)
{
uint16_t buttons;
buttons = ((( GPIOB->IDR & ( GPIO_IDR_IDR8 )) >> 8 ) | (( GPIOB->IDR & ( GPIO_IDR_IDR9 )) >> 8 ) \
| (( GPIOC->IDR & ( GPIO_IDR_IDR12 )) >> 10 ) | (( GPIOA->IDR & ( GPIO_IDR_IDR5 )) >> 2 )) \
& 0x0F;
return (buttons);
}
 //Control LED will turn on the 4 red LED based on the main code
//**NOTE** Currently overwrites whatever else may be in the output register already and replaces
//				 with values that only control the LED's.
void controlLED(uint16_t LEDbits){
	LEDbits = LEDbits << 9;
	GPIOA->ODR = LEDbits;
}

//getdipsw is based on the example code given on URcourses and talked about in class. 
uint16_t getdipsw(void){
	uint16_t dipsw;
	dipsw = ((( GPIOA->IDR & ( GPIO_IDR_IDR6)) >> 3) | (( GPIOC->IDR & ( GPIO_IDR_IDR10)) >> 9) | (( GPIOA->IDR & (GPIO_IDR_IDR7)) >> 5)) | (( GPIOC->IDR & ( GPIO_IDR_IDR11 )) >> 11)\
	& 0x0F;
return (dipsw);
}

void USART3Init(void){
	//set PG10 and 11 to Alternate function outputs. 
	RCC->APB1ENR |= RCC_APB1ENR_USART3EN;
	
	GPIOB->CRH |= GPIO_CRH_MODE11 | GPIO_CRH_MODE10;
	GPIOB->CRH &= ~GPIO_CRH_CNF11_0 & ~GPIO_CRH_CNF10_0;
	GPIOB->CRH |= GPIO_CRH_CNF11_1 | GPIO_CRH_CNF10_1;
	
	
	
	USART3->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE; //sets usart enable 1, TE 1, RE 1
	USART3->BRR |= 0x9C4; // sets the baud rate to 9600
	USART3->CR1 |= USART_CR1_TE | USART_CR1_RE; 
	
}

void UARTSend(uint8_t data){
	USART3->DR = data;
}