/******************************************************************************
 * Name: Clock.h
 * Description: Will handle clock setup
 * Version: V1.00
 * Author: Thomas Martineau
 *****************************************************************************/
 #include <stdint.h>
 
 //Functions
 
 void clkInit(void);
 
 // A general purpose countdown timer delay routine (Taken from Lab 1 example)
  void delay(uint32_t delay);
 
 //Initialize tim 1 with 50% DC
 void TIM1Init(void);