/******************************************************************************
 * Name:    GPIO.h
 * Description: Will handle GPIO initialization
 * Version: V1.00
 * Author: Thomas Martineau
 *****************************************************************************/
  #include <stdint.h>
	
	//Functions
	//Initializes the GPIO ports
	void GPIOInit(void);
	
	//Initializes the LED's
	void LEDInit(void);
	
	//Will return a uint_16t with the last 4 bits related to the state of the pushbuttons
	uint16_t getpushbuttons(void);
	
	//Will control the 4 red LED. Needs a uint16_t with in the state 0x000X where X are the 4 bits controlling the LEDs
	void controlLED(uint16_t);
	
	//Will return a uint_16t with the last 4 bits related to the state of the dipsw
	uint16_t getdipsw(void);
	
	void discoveryIOInit(void);
	
	//initialize tim1
	void TIM1CLKInit(void);
	
	//initialize USART
	void USART3Init(void);
	void UARTSend(uint8_t);