/******************************************************************************
 * Name: Clock.c
 * Description: Will handle clock setup and timing functions
 * Version: V1.00
 * Author: Thomas Martineau
 *****************************************************************************/
 #include "stm32f10x.h"
 #include "Clock.h"
 
 //Clock initialize function. Template taken from lab1_lib.c
 void clkInit(void){
	 uint32_t temp = 0x00;
	 
	 RCC->CFGR = RCC_CFGR_PLLMULL_3 | RCC_CFGR_SW_PLL; //This selects the PLL clock, and sets the xfactor to x3
	 RCC->CR = RCC_CR_PLLON | RCC_CR_HSION | RCC_CR_HSEON | 0x00000080; //Turns the PLL, HSE, and HSI on. Trims HSI to 8MHz
	 
	 //checks if PLL is rdy by checking PLL rdy bit
	 while (temp != RCC_CR_PLLRDY){
		 temp = RCC->CR & RCC_CR_PLLRDY; //ands with PLL rdy bit to check if its ready
	 }
 }
 
 void delay(uint32_t count)
{
    int i=0;
    for(i=0; i< count; ++i)
    {
    }
}

void TIM1Init(void){
	TIM1->CR1 |= TIM_CR1_CEN; //Enable Timer 1
	TIM1->CR2 |= TIM_CR2_OIS1; //Output Idle State for channel 1 OC1=1 when MOE=0 (Setting output idle state to high)
	TIM1->EGR |= TIM_EGR_UG; // Reinitialize the counter (EGR = event generation register, UG = Update generation)
	TIM1->CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1PE | TIM_CCMR1_OC1FE; 
		//OC1M = Output compare 1 mode by enabling 1 & 2 bit it is put into PWM mode 1. So CH1 active when Tim1_CNT < TIM1_CCR1. 
		//OC1PE = Output Compare 1 Preload Enable. 1 = enabled
		//OC1FE = Output Compare 1 Fast Enable
	TIM1->CCER |= TIM_CCER_CC1E; // Enable the CH1 output on PA8
	TIM1->PSC = 0x095F;  //Divide 24MHz by 2400, PSC_CLK = 10 000Hz, 1 CNT = 0.1mS
	TIM1->ARR = 100; //100 counts = 10 mS
	TIM1->CCR1 = 50; //50 counts = 1mS = 50% Duty Cycle
	TIM1->BDTR |= TIM_BDTR_MOE | TIM_BDTR_OSSI; // MOE = Main Output Enable, OSSI = Off state selection for Idle mode. Force Idle Level First 
	TIM1->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; //enable timer 1
	
}