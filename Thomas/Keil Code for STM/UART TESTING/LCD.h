 /******************************************************************************
 * Name:    LCD.h
 * Description: Will handle LCD initialization
 * Version: V1.00
 * Author: Thomas Martineau
 *****************************************************************************/
  #include <stdint.h>
	
#define LCD_8B2L 0x38 // ; Enable 8 bit data, 2 display lines
#define LCD_DCB 0x0F // ; Enable Display, Cursor, Blink
#define LCD_MCR 0x06 // ; Set Move Cursor Right
#define LCD_CLR 0x01 // ; Home and clear LCD
#define LCD_LN1 0x80 // ;Set DDRAM to start of line 1
#define LCD_LN2 0xC0 // ; Set DDRAM to start of line 2

#define LCD_CM_ENA 0x00210002 
#define LCD_CM_DIS 0x00230000 
#define LCD_DM_ENA 0x00200003 
#define LCD_DM_DIS 0x00220001 
	
 void LCD_IO_Init(void);
 
 void LCD_INIT(void);
 
 void commandToLCD(uint8_t data);
 
 void dataToLCD(uint8_t data);
 
 uint8_t hex2ascii(uint8_t);
 
 void outputint32(uint32_t displayvalue);
 