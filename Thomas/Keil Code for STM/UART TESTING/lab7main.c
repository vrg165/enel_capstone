/*Lab 7 Main*/
/* Version 1.00 */
/* Author - Thomas Martineau, March 2nd, 2020 */

#include "stm32f10x.h"
#include "Clock.h"
#include "GPIO.h"
#include "LCD.h"
int main(void){
	clkInit();
	GPIOInit();
	USART3Init();
	discoveryIOInit();
	LEDInit();
	uint16_t start = 0x41;
	uint16_t endtest = 0x7E;
	uint16_t statustest = 0x0;
	uint16_t data = 0x0;
	
	while (1){
		
		statustest = USART3->SR;
		int gtr = (statustest & 0x20); //checks the RxE bit. 
		int gtt = statustest & 0xC0; //checks the Txe and TC bit.
		
		if (gtr == 0x0020){
		data = USART3->DR;
		while ((USART3->SR & 0x20) != 0x0){
		}
			
			if (data == 0x0){
			GPIOC->ODR |= GPIO_ODR_ODR8 | GPIO_ODR_ODR9;				
			}
			else if (data == 0x31){
			GPIOC->ODR ^= GPIO_ODR_ODR8;	
			}
			else if (data == 0x59){
			GPIOC->ODR ^= GPIO_ODR_ODR9;	
			}
		}
		
		if (gtt == 0x00c0){
			
			USART3->CR1 |= USART_CR1_TE;
			UARTSend(start);
			while((USART3->SR & 0x40) != 0x40){
			}
			start = 0x55;
//			if (start < 0x7E){
//			start = start+1;
//			}
//			else if (start == 0x7E){
//			start = 0x21;
//			}
//			
			delay(10000);
		}
	}
}

	
