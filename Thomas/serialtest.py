import serial
import time
import datetime
import csv

serialport = serial.Serial()
serialport.baudrate = 9600
serialport.bytesize = serial.EIGHTBITS
serialport.port = "/dev/serial0"
serialport.stopbits=serial.STOPBITS_ONE
serialport.parity = serial.PARITY_EVEN

serialport.open()
i = 0
temp = 0
sp02 = 0
users = 0
serialport.flushInput()
day = time.strftime("%a %d %b")
print(day)
newDay = 0;
while True:
	if day != newDay:
		with open("test_data.csv","a") as f:
			writer = csv.writer(f,delimiter=",")
	input = serialport.read()
	#print (input)
	if input!= None:
		#print (input)
		if input == '1':
			#print (input)
			temp = serialport.read(1)
			sp02 = serialport.read(1)
			users = users+1
			daytime = time.asctime(time.localtime(time.time()))
			print('New User -> Temperature: {}. SP02 {}. Total Users {}. Date/Time {}'.format(temp,sp02,users,daytime))
			writer.writerow([temp,sp02,users])
#	while i < 5000: 
#		serialport.write(b'\x31')
#		i+= 1
#		time.sleep(0.1)
#	else:
#		s = serialport.read(300)
#		f.write(s)
#		f.close()
