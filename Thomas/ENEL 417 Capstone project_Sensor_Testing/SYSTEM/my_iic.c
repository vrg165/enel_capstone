/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//
//	This Program is run on the main STM32F100RB microcontroller. 
//	This is the I2C definition file, which states all the definitions 
//	and requirements of the IIC we are using
//	
******************************************************************************************/
#include "my_iic.h"

//initialize iic
void IIC_Init(void)
{
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SCL,GPIO_Mode_Out_PP);
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_Out_PP);

#endif
    PinOut(PIN_IIC_SCL)=1;
    PinOut(PIN_IIC_SDA)=1;
}
// Generate iic start signal
void IIC_Start(void)
{
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_Out_PP);

#endif
    PinOut(PIN_IIC_SDA)=1;
    PinOut(PIN_IIC_SCL)=1;
    delay_us(4);
    PinOut(PIN_IIC_SDA)=0;//START:when CLK is high,DATA change form high to low 
    delay_us(4);
    PinOut(PIN_IIC_SCL)=0;//ready to transmit
}
//Generate iic stop signal
void IIC_Stop(void)
{
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_Out_PP);//sda output

#endif
    PinOut(PIN_IIC_SCL)=0;
    PinOut(PIN_IIC_SDA)=0;//STOP:when CLK is high DATA change form low to high
    delay_us(4);
    PinOut(PIN_IIC_SCL)=1; 
    PinOut(PIN_IIC_SDA)=1;//send iic stop signal
    delay_us(4);
}
//wait for ACK signal
//return value ��1��receiving failed
//        0��receiving success
bool IIC_Wait_Ack(void)
{
    u8 ucErrTime=0;
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_IPU);//set SDA to input

#endif
    PinOut(PIN_IIC_SDA)=1;delay_us(1);
    PinOut(PIN_IIC_SCL)=1;delay_us(1);
    while(PinRead(PIN_IIC_SDA))
    {
        ucErrTime++;
        if(ucErrTime>248)
        {
            IIC_Stop();
            return false;
        }
    }
    PinOut(PIN_IIC_SCL)=0;// clock output =0
    return true;
} 
//Generate ACK, entry parameters : ack (0:ACK 1:NAK) 
void IIC_SendAck(Bit_IICACK ack)
{
    PinOut(PIN_IIC_SCL)=0;
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_Out_PP);//sda line output
#endif
    PinOut(PIN_IIC_SDA)=(u8)ack;
    delay_us(2);
    PinOut(PIN_IIC_SCL)=1;
    delay_us(2);
    PinOut(PIN_IIC_SCL)=0;
}

//IIC send one bit
//return true/false
//true��ACK
//false��NAK
u8 IIC_Send_Byte(u8 sendDat)
{
    u8 t;
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_Out_PP);//sda output

#endif
    PinOut(PIN_IIC_SCL)=0;// set scl=0 starting transmit
    for(t=0;t<8;t++)
    {
        PinOut(PIN_IIC_SDA)=(sendDat&0x80)>>7;
        sendDat<<=1;
        delay_us(2);//this delay is necessary for TEA 5767
        PinOut(PIN_IIC_SCL)=1;
        delay_us(30); 
        PinOut(PIN_IIC_SCL)=0;
        delay_us(30);
    }
    return IIC_Wait_Ack();
}
//read one bit, when ack=1 send ACK, when ack=0 send nACK
u8 IIC_Recv_Byte(Bit_IICACK ack)
{
    u8 i,receive=0;
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(PIN_IIC_SDA,GPIO_Mode_IPU);//set SDA to input
#endif
    for(i=0;i<8;i++ )
    {
        PinOut(PIN_IIC_SCL)=0; 
        delay_us(2);
        PinOut(PIN_IIC_SCL)=1;
        receive<<=1;
        if(PinRead(PIN_IIC_SDA))receive++;   
        delay_us(1); 
    }
    IIC_SendAck(ack); //Send ACK
    return receive;
}

//Write register
//SlaveAddress
//REG_Address
// data will write in the register
//no return value
void IIC_WriteOneByte(u8 SlaveAddress, u8 REG_Address,u8 REG_data) 
{
    IIC_Start();//Generate IIC start signal
    IIC_Send_Byte(SlaveAddress);//Send slave Address
    IIC_Send_Byte(REG_Address);//send register address
    IIC_Send_Byte(REG_data);//send value to register
    IIC_Stop();//generate IIC stop signal
}
//*********************************************************
void IIC_WriteBytes(u8 SlaveAddress, u8 REG_Address,u8 *buf,u16 len)
{
    IIC_Start();//Generate IIC start signal
    IIC_Send_Byte(SlaveAddress);//Send slave Address
    IIC_Send_Byte(REG_Address);//send register address
    while(len--)
    {
        IIC_Send_Byte(*buf++);//send data
    }
    IIC_Stop();//generate IIC stop signal
}
void IIC_WriteString(u8 SlaveAddress, u8 REG_Address,char *buf)
{
    IIC_WriteBytes(SlaveAddress,REG_Address,(u8 *)buf,strlen(buf));
}
//read register
//SlaveAddress
//REG_Address
//return: value read
u8 IIC_ReadOneByte(u8 SlaveAddress, u8 REG_Address)
{
    u8 temp=0;
    IIC_Start();//Generate IIC start signal
    IIC_Send_Byte(SlaveAddress);//Send slave Address
    IIC_Send_Byte(REG_Address);//send register address
    
    IIC_Start();//Restart; Generate IIC start signal
    IIC_Send_Byte(SlaveAddress+1);//Send read register command
    temp=IIC_Recv_Byte(IIC_BIT_NOACK);//Read 1 bit, stop reading send NAK
    IIC_Stop();//generate IIC stop signal
    return temp;//return value read
}

//*********************************************************
void IIC_ReadBytes(u8 SlaveAddress, u8 REG_Address,u8 *buf,u16 len)
{
    IIC_Start();//Generate IIC start signal
    IIC_Send_Byte(SlaveAddress); //send slave address and write signal
    IIC_Send_Byte(REG_Address);//send register address
    IIC_Start();//Generate IIC start signal
    IIC_Send_Byte(SlaveAddress+1);//send slave address and read signal
    while(--len)
    {
        *buf++ = IIC_Recv_Byte(IIC_BIT_ACK);
    }
        *buf++ = IIC_Recv_Byte(IIC_BIT_NOACK);
    IIC_Stop();//generate IIC stop signal
}

