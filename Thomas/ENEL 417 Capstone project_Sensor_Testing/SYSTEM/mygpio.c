/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//	referenced from stm32f10x library
//	
//	
******************************************************************************************/

#include "mygpio.h"

#define VERSION     13

void GPIO_Pin_Init(MyPinDef pin,GPIOMode_TypeDef Mode)
{
    GPIO_InitTypeDef GPIO_InitStructure;//declare structure
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA<<(pin>>4), ENABLE);//enable GPIO
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; //50M clock speed
    if(pin==PB3 || pin==PB4 || pin==PA15)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
        GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);//close jtag�� enable swd��
    }
    GPIO_InitStructure.GPIO_Pin  = GET_PIN_GPIO(pin); 
    GPIO_InitStructure.GPIO_Mode = Mode;
    GPIO_Init(GET_PORT_GPIO(pin), &GPIO_InitStructure);
}

//BSRR for high register, 1 is low ; for low register 0 is high.
//write 1 to the high register
void GPIO_WriteHigh(GPIO_TypeDef* GPIOx,u8 dat)
{
    GPIOx->BRR = 0xff00;
    GPIOx->BSRR = dat<<8;
}
//write to low register
void GPIO_WriteLow(GPIO_TypeDef* GPIOx,u8 dat)
{
    GPIOx->BRR = 0x00ff;
    GPIOx->BSRR = dat;
}
u16 My_GPIO_GetVersion(void)
{
    return VERSION;
}
