/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	April 12 2021
//
//	Zhengyang Zhang
//	function headers for max301xxuart.c
//
//	
******************************************************************************************/

#ifndef __MY_MAX301XX_UART_H
#define __MY_MAX301XX_UART_H
#include "my_include.h"

extern u8 SPO2_max301xx;


void My_MAX301xx_Init(USART_TypeDef* USARTx);
void My_MAX301xx_Uart_StateMachine(u8 msgByte);
void My_MAX301xx_Uart_GetSPO2(USART_TypeDef* USARTx);
#endif
