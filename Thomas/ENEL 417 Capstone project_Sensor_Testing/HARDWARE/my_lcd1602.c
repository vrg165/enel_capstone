/****************************************************************************************
//	ENEL 417 Capstone Project: Group 7
//	Health Measurement Station
//
//	
//	March 2021
//
//	Zhengyang Zhang
//
//	This Program is run on the main STM32F100RB microcontroller. 
//	The program initilize the LCD pins and be able to make the LCD to work
//	along with user measurement data management.
//
//	
//	
//
//************************************************************************/
#include "my_lcd1602.h"

void LCD_GPIO_init(void)
{
    u8 i;
#if !defined (USE_HAL_DRIVER)
    for(i=0;i<8;i++)
    {
        GPIO_Pin_Init(Pins_Data_1602[i],GPIO_Mode_Out_PP);
    }
    GPIO_Pin_Init(PIN_EN,GPIO_Mode_Out_PP);
    GPIO_Pin_Init(PIN_RW,GPIO_Mode_Out_PP);
    GPIO_Pin_Init(PIN_RS,GPIO_Mode_Out_PP);

#endif
}
void GPIO_data(u8 x)  
{
    u8 i;
    for(i=0;i<8;i++)
    {
        if(x&(0x01<<i))
        {
            PinSet(Pins_Data_1602[i]);//DB0
        }
        else{
            PinReset(Pins_Data_1602[i]);//DB0
        }
    }
}

void LCD_En_Toggle(void) // Enable paluse
{
    SET_EN;
    delay_us(5);//delay 160us
    CLE_EN;
}

void LCD_Busy(void)
{
    u8 i;
    u16 later0=0;
    for(i=0;i<8;i++)
    {
#if !defined (USE_HAL_DRIVER)
    GPIO_Pin_Init(Pins_Data_1602[i],GPIO_Mode_IPU);

#endif
    }

     CLR_RS;//RS = 0
     SET_RW;//RW = 1
     SET_EN;//EN = 1

     while ((PinRead(Pins_Data_1602[7]))&&(later0<20000)) 
     {
        delay_us(5);
        later0++;
     }
     CLE_EN;//EN = 0

     // restore pin to output mode
    for(i=0;i<8;i++)
    {
#if !defined (USE_HAL_DRIVER)
        GPIO_Pin_Init(Pins_Data_1602[i],GPIO_Mode_Out_PP);

#endif
    }
}

//write command to LCD: RS=L,RW=L,Data0-Data7
void LCD1602_WriteCmd(u8 x,char cmd) 
{
    if(cmd) LCD_Busy();
    
    CLR_RS;//RS = 0 
    CLE_RW;//RW = 0   
    GPIO_data(x);
    LCD_En_Toggle();
    LCD_Busy();

}
void LCD1602_WriteData(u8 x) 
{

    SET_RS;//RS = 1   
    CLE_RW;//RW = 0  
    GPIO_data(x);  
    LCD_En_Toggle();
    LCD_Busy();
}

void LCD_SetXY(u8 x,u8 y)   
{ 
     u8 addr;
     if(y==0) 
          addr=0x80+x;
     else if(y==1)
          addr=0xC0+x;
     LCD1602_WriteCmd(addr,1) ;
}

void LCD1602_Init( void )
{
    LCD_GPIO_init();

    LCD1602_WriteCmd( 0x38,1);//display mode setting
    LCD1602_WriteCmd( 0x08,1);//display off
    LCD1602_WriteCmd( 0x01,1);//clear display
    LCD1602_WriteCmd( 0x06,1);//display cursor move setting
    LCD1602_WriteCmd( 0x0C,1);//display on, not cursor
}
 
void LCD1602_Write_Char(u8 x,u8 y,const char Data0)
{
    LCD_SetXY(x,y);
    LCD1602_WriteData(Data0);
}
void LCD1602_Write_String(u8 x,u8 y,const char *string) // write string to LCD
{
    
    LCD_SetXY(x,y);
    while(*string) 
    {
        LCD1602_WriteData(*string);
        string++;
    }
}

