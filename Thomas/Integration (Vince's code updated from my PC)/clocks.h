
#ifndef CLOCKS_H
#define CLOCKS_H


#include "includeList.h"

// set SysTickLoad = 15,000,000
// with 3MHz clock counter will reach 0 and interrupt every 5 seconds
#define SysTickLoad 0xE4E1C0

void clock_init(void);
void delay(uint32_t count);
void sysTick_init(void);

#endif
