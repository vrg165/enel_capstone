#include "usart.h"
//
//	March 2021
//	Vincent Grabowski
//
//	Initializes USART 2 and 3 - baud rate set to 9600 bits per second
void USART_init()
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN;
	RCC->AHBENR |= RCC_APB2ENR_AFIOEN ;
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;
	
	RCC->APB1ENR |= RCC_APB1ENR_USART3EN | RCC_APB1ENR_USART2EN;
	
	//set PB10 and PA2 as alternative function output push-pull
	GPIOB->CRH |= GPIO_CRH_MODE10 | GPIO_CRH_CNF10_1;
  GPIOB->CRH &= ~GPIO_CRH_CNF10_0 ;
	
	GPIOA->CRL |= GPIO_CRL_MODE2 | GPIO_CRL_CNF2_1;
	GPIOA->CRL &= ~GPIO_CRL_CNF2_0;
	
	//set PB11 and PA3 as alternative function output push-pull
	GPIOB->CRH |= GPIO_CRH_MODE11 | GPIO_CRH_CNF11_1;
  GPIOB->CRH &= ~GPIO_CRH_CNF11_0 ;
	
	GPIOA->CRL |= GPIO_CRL_MODE3 | GPIO_CRL_CNF3_1;
	GPIOA->CRL &= ~GPIO_CRL_CNF3_0;
	
	//USART and Tx Rx Enabled
	USART3->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	//set baud rate 9600 bits per second
	USART3->BRR |= 0x09C4;
	USART2->BRR |= 0x09C4;
	USART3->CR1 |= USART_CR1_TE | USART_CR1_RE;
	USART2->CR1 |= USART_CR1_TE | USART_CR1_RE;
	
}

void USART_TX_Byte(USART_TypeDef* USARTx, uint8_t data)
{
	USARTx->CR1 |= USART_CR1_TE;
	volatile uint32_t SR = USARTx->SR;
	USARTx->DR = data;
	SR = USARTx->SR;
	while((USARTx->SR & 0x40) == 0){}
}
uint8_t USART_RX_Byte(USART_TypeDef* USARTx)
{
	volatile uint8_t data;
	data = USARTx->DR;
	while((USARTx->SR & 0x20) != 0){}
	return data;
}

//	March 2021
//	Vincent Grabowski
//
//	Function called when user data is sent from second controller.
//	will always recieve 5 bytes in a row. first byte is SPO2 data, 
//	next 4 bytes are temperature data. all data recieved stored in
//	array "data" passed to function. 
//	Function ends after 5th byte has been recieved and stored.
void USART_RX_User_Data(USART_TypeDef* USARTx, uint8_t *data)
{
	volatile uint8_t i = 0;
	while(i < 5)
	{
		if((USARTx->SR & 0x20) == 0x20)
		{
			data[i] = USART_RX_Byte(USARTx);
			i++;
		}
	}
}

//
//	March 2021
//	Vincent Grabowski
//	
//	Function called in order to store 10 consecutive measurements
//	seperated and stored into SPO2 and TEMP arrays passed to function.
//	We know that the RX pin will recieve 5 bytes at a time, and the 
//	measurements are sent every 100 milli-seconds. 
//	Function ends after 10th measurement is recieved and stored.
void get_10_Measure(USART_TypeDef* USARTx, uint8_t *SPO2, float *TEMP)
{
	volatile int i = 0;
	uint8_t data[5];
	char temp[4];
	
	while(i < 10)
	{
		if((USARTx->SR & 0x20) == 0x20)
		{
			USART_RX_User_Data(USARTx, data);
			SPO2[i] = data[0];
			
			temp[0] = data[1];
			temp[1] = data[2];
			temp[2] = data[3];
			temp[3] = data[4];
			sscanf(temp, "%f", &TEMP[i]);
			
			i++;
		}
	}
}
