//
//	March 2021
//	Vincent Grabowski
//
//	Functions Used in system User Display -
//	Controls LCD screen and notification LEDs
//
//	LCD control functions created in ENEL 387 Lab
//	with instruction from lab instructor Dave Duguid
//
#include "userDisplay.h"

//send 8 bit command to LCD
void commandToLCD(uint8_t data)
{
	GPIOB->BSRR = LCD_CM_ENA; //RS low, E high
	// GPIOC->ODR = data; //BAD: may affect upper bits on port C
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	delay(8000);
	GPIOB->BSRR = LCD_CM_DIS; //RS low, E low
	delay(80000);
}
//display 8 bit data at cursor position on LCD
void dataToLCD(uint8_t data)
{
	GPIOB->BSRR = LCD_DM_ENA; //RS low, E high
	// GPIOC->ODR = data; //BAD: may affect upper bits on port C
	GPIOC->ODR &= 0xFF00; //GOOD: clears the low bits without affecting high bits
	GPIOC->ODR |= data; //GOOD: only affects lowest 8 bits of Port C
	delay(8000);
	GPIOB->BSRR = LCD_DM_DIS; //RS low, E low
	delay(80000);
}	
//prints 32 bit value in Hex characters
void display32int(uint32_t displayValue)
{
	int i;
	uint32_t shiftedDisplayValue;
	uint8_t outputValue;
	dataToLCD(0x30);
	dataToLCD(0x78);
	
	for(i=28; i >= 0; i = (i - 4))
	{
		shiftedDisplayValue = ((displayValue >> i) & 0x0F);
		outputValue = (convertToASCII(shiftedDisplayValue));
		dataToLCD(outputValue);
	}
	
	dataToLCD(0x20);
}
//initialize LCD screen
//Enables Display - No Cursor - No Blinking
//Sets to first position and clears LCD
void LCD_init(void)
{	
	commandToLCD(0x38);
	commandToLCD(0x38);
	commandToLCD(0x38);
	commandToLCD(0x38);
	commandToLCD(0x0C);
	commandToLCD(0x01);
	commandToLCD(0x06);
	
}
//enables IO pins for LCD, then calls LCD_init() function above
void LCD_IO_init(void)
{
	//start clocks for port C and B
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN | RCC_APB2ENR_IOPBEN | RCC_APB2ENR_IOPAEN;
	
	GPIOB->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE5 ;
  GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF5 ;
	
	GPIOC->CRL |= GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3 \
						 | GPIO_CRL_MODE4 | GPIO_CRL_MODE5 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7 ;
	GPIOB->CRL &= ~GPIO_CRL_CNF0 & ~GPIO_CRL_CNF1 & ~GPIO_CRL_CNF2 & ~GPIO_CRL_CNF3 \
						 & ~GPIO_CRL_CNF4 & ~GPIO_CRL_CNF5 & ~GPIO_CRL_CNF6 & ~GPIO_CRL_CNF7 ;
	
	GPIOA->CRH |= GPIO_CRH_MODE9;
	GPIOA->CRH &= ~GPIO_CRH_CNF9;
	GPIOA->ODR |= GPIO_ODR_ODR9;
	
	delay(80000);
	LCD_init();
	
}
ADC1//converts a 8bit number to ascii hex char 0-F
uint8_t convertToASCII(uint16_t data)
{
	uint8_t temp = (data & 0x0F);
	
	if(temp < 0x0A)
	{
		return(temp + 0x30);
	}
	else
	{
		return(temp + 0x37);
	}
}
//prints a string of char to LCD screen
void strToLCD(char str[])
{
	uint16_t i = 0;
	uint16_t data;
	
	while(str[i])
	{
		data = str[i];
		
		dataToLCD(data);
		i = i + 1;
	}
}

void reg_out(uint32_t reg_data)
{
	int i;
	uint32_t shifted_val;
	uint8_t print_val;
	
	dataToLCD(0x30);
	dataToLCD(0x78);
	
	for(i=28; i>=0; i=(i-4))
	{
		shifted_val = (reg_data >> i) & 0x0F;
		print_val = (convertToASCII(shifted_val));
		dataToLCD(print_val);
	}
	
	dataToLCD(0x20);
}
//
//	March 2021
//	Vincent Grabowski
//
//	Function called and beginning of Ready state
//	Prints a message to user providing instrucitons
void LCD_Welcome(void)
{
		commandToLCD(LCD_CLR);
		strToLCD("Place Finger");
		commandToLCD(LCD_LN2);
		strToLCD("On Scanner");
}

//	
//	March 2021
//	Vincent Grabowski
//
//	Function called in the event that users SPO2 and TEMP 
//	are within defined acceptable value range. Takes the
//	SPO2 and TEMP values passed to function and displays
//	the results on LCD. Also turns on green LED (PA10 = 0)
void printGood(uint8_t SPO2, float TEMP)
{
	char display[32];
	commandToLCD(LCD_CLR);
	sprintf(display," Temp:%4.1fC    ",TEMP);
	strToLCD(display);
	commandToLCD(LCD_LN2);
	sprintf(display," SPO2:%03d ", SPO2);
	strToLCD(display);
	GPIOA->ODR &= ~GPIO_ODR_ODR10;
}

//	
//	March 2021
//	Vincent Grabowski
//
//	Function called in the event that users SPO2 and TEMP 
//	are outside defined error threshold value range. Takes the
//	SPO2 and TEMP values passed to function and asks user to 
//	scan again. Will check which sensor gave the bad measurement 
// 	and indicate cause to user. Also turns on yello LED (PA11 = 0)
void printError(uint8_t SPO2, float TEMP)
{
	char display[32];
	
	commandToLCD(LCD_CLR);
	strToLCD(" SCAN AGAIN ");
	commandToLCD(LCD_LN2);
	if(SPO2 < BAD_SPO2_READ){
		sprintf(display," SPO2 ERR. %03d ", SPO2);
		strToLCD(display);
	}
	else if(TEMP < BAD_TEMP_READ){
		sprintf(display," TEMP ERR. %4.1fC ", TEMP);
		strToLCD(display);
	}
	GPIOA->ODR &= ~GPIO_ODR_ODR11;
}

//	
//	March 2021
//	Vincent Grabowski
//
//	Function called in the event that users SPO2 and TEMP 
//	are outside defined acceptable value range but unlikely
// 	the result of a bad scan (Fever or slightly low SPO2).
//	Indicates cause of Red LED result on LCD screen. Takes the
//	SPO2 and TEMP values passed to function and displays
//	the results on LCD. Also turns on red LED (PA12 = 0)
void printBad(uint8_t SPO2, float TEMP)
{
	char display[32];
	commandToLCD(LCD_CLR);
	if(TEMP > HIGH_TEMP)
		sprintf(display," High Temp:%4.1fC    ", TEMP);
	else
		sprintf(display," Temp:%4.1fC    ", TEMP);
	strToLCD(display);
	commandToLCD(LCD_LN2);
	if(SPO2 < LOW_SPO2)
		sprintf(display," Low SPO2:%03d ", SPO2);
	else
		sprintf(display," SPO2:%03d ", SPO2);
	strToLCD(display);
	GPIOA->ODR &= ~GPIO_ODR_ODR12;
}

//
//	March 2021
//	Vincent Grabowski
//
//	Initialize IO pins required for LEDs, LCD backlight control, 
//	and measurement transmit control
void GPIO_PIN_init(void)
{
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	
	//Set Pins PA1, PA8, PA9, PA10, PA11, and PA12 to outputs (50MHz)
	GPIOA->CRL |= GPIO_CRL_MODE1;
	GPIOA->CRH |= GPIO_CRH_MODE8 | GPIO_CRH_MODE9 | GPIO_CRH_MODE10 | GPIO_CRH_MODE11 | GPIO_CRH_MODE12;
	GPIOA->CRL &= ~GPIO_CRL_CNF1 ;
	GPIOA->CRH &= ~GPIO_CRH_CNF8 & ~GPIO_CRH_CNF9 & ~GPIO_CRH_CNF10 & ~GPIO_CRH_CNF11 & ~GPIO_CRH_CNF12;
	GPIOA->ODR |= GPIO_ODR_ODR10 | GPIO_ODR_ODR11 | GPIO_ODR_ODR12;
}

//
//	March 2021
//	Vincent Grabowski
//
//	Function has an array of size 10 passed to it containing
//	10 consecutive user SPO2 measurements(taken 100ms apart).
//	Calculates average of 10 values in SPO2 array, removing 
//	highest and lowest values. Returns Average SPO2 value as
//	a uint8_t.
uint8_t calcAvgSPO2(uint8_t *SPO2)
{
	uint32_t avgSPO2 = SPO2[0];
	uint8_t minSPO2 = SPO2[0];
	uint8_t maxSPO2 = SPO2[0];
	for(int i = 1; i < 10; i++)
	{
		avgSPO2 = avgSPO2 + SPO2[i];
		if(SPO2[i] < minSPO2)
			minSPO2 = SPO2[i];
		else if(SPO2[i] > maxSPO2)
			maxSPO2 = SPO2[i];
	}
	avgSPO2 = ((avgSPO2 - minSPO2 - maxSPO2) / 8) & 0xFFFF;
	return avgSPO2;
}
//
//	March 2021
//	Vincent Grabowski
//
//	Function has an array of size 10 passed to it containing
//	10 consecutive user TEMP measurements(taken 100ms apart).
//	Calculates average of 10 values in TEMP array, removing 
//	highest and lowest values. Returns Average TEMP value as
// 	a float.
float calcAvgTEMP(float *TEMP)
{
	float avgTEMP = TEMP[0];
	float minTEMP = TEMP[0];
	float maxTEMP = TEMP[0];
	for(int i = 1; i < 10; i++)
	{
		avgTEMP = avgTEMP + TEMP[i];
		if(TEMP[i] < minTEMP)
			minTEMP = TEMP[i];
		else if(TEMP[i] > maxTEMP)
			maxTEMP = TEMP[i];
	}
	avgTEMP = (avgTEMP - minTEMP - maxTEMP) / 8;
	return avgTEMP;
}
